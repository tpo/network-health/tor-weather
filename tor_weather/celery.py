from tor_weather.extensions import celery

from .app import app

celery = celery
celery.autodiscover_tasks(
    ["tor_weather.tasks.send_email", "tor_weather.tasks.onionoo", "tor_weather.tasks.test"]
)
app.app_context().push()
