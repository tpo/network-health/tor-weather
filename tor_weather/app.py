from flask import Flask

from tor_weather import create_app

app: Flask = create_app()

if __name__ == "__main__":
    app.run(port=app.config.get("FLASK_RUN_PORT"))
