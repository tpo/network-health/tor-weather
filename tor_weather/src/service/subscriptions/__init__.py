from abc import ABC, abstractmethod

from tor_weather.src.dto.onionoo import OnionooRelayInterface


class SubscriptionBaseSvc(ABC):
    @abstractmethod
    def validate(self, subscription_id: int, relay: OnionooRelayInterface) -> None:
        pass
