from datetime import datetime

from flask import url_for

from tor_weather.database.subscriptions.node_bandwidth_sub import NodeBandwidthSub
from tor_weather.error import BadRequest
from tor_weather.src.constants.common import SnackbarMessage
from tor_weather.src.dao.relay import RelayDAO
from tor_weather.src.dao.subscription import SubscriptionDAO
from tor_weather.src.dao.subscriptions.node_bandwidth import NodeBandwidthDAO
from tor_weather.src.dto.onionoo import OnionooNodeInterface
from tor_weather.src.dto.presentation.table import (
    ITable,
    ITableHeader,
    ITableIconCell,
    ITableIconCellObject,
    ITableTextCell,
)
from tor_weather.src.dto.response import RedirectResponseDTO
from tor_weather.src.dto.subcriptions.node_bandwidth import (
    INodeBandwidthTableRow,
    NodeBandwidthSubDTO,
)
from tor_weather.src.dto.subscription import InsertSubscriptionDTO
from tor_weather.src.service.email.node_bandwidth import NodeBandwidthEmailSvc
from tor_weather.src.service.subscriptions import SubscriptionBaseSvc
from tor_weather.utilities import time_diff


class NodeBandwidthSubscriptionSvc(SubscriptionBaseSvc):
    @staticmethod
    def get_subscriptions(email: str) -> ITable:
        """Get subscriptions for a user

        Args:
            email (str): Email of the user

        Returns:
            List[UINodeBandwidthListItem]: List of subscriptions
        """
        subscriptions = NodeBandwidthDAO.get_subs_for_user(email)
        return ITable(
            headers=[
                ITableHeader(label="Fingerprint", value="fingerprint"),
                ITableHeader(label="Wait For", value="wait_for"),
                ITableHeader(label="Threshold", value="threshold"),
                ITableHeader(label="Active", value="is_active"),
                ITableHeader(label="", value="edit"),
                ITableHeader(label="", value="delete"),
            ],
            content=[
                INodeBandwidthTableRow(
                    fingerprint=ITableTextCell(text=s.subscription.relay.fingerprint),
                    wait_for=ITableTextCell(text=f"{s.wait_for} hrs"),
                    threshold=ITableTextCell(text=f"{s.threshold} KBps"),
                    is_active=ITableIconCell(
                        icon=(
                            ITableIconCellObject(
                                name="check_box",
                                type="filled",
                                url=url_for(
                                    "node_bandwidth_disable_api",
                                    fingerprint=s.subscription.relay.fingerprint,
                                ),
                            )
                            if s.is_active
                            else ITableIconCellObject(
                                name="check_box_outline_blank",
                                type="outlined",
                                url=url_for(
                                    "node_bandwidth_enable_api",
                                    fingerprint=s.subscription.relay.fingerprint,
                                ),
                            )
                        )
                    ),
                    edit=ITableIconCell(
                        icon=ITableIconCellObject(
                            name="edit",
                            type="outlined",
                            url=url_for(
                                "node_bandwidth_modify",
                                fingerprint=s.subscription.relay.fingerprint,
                            ),
                        )
                    ),
                    delete=ITableIconCell(
                        icon=ITableIconCellObject(
                            name="delete",
                            type="outlined",
                            url=url_for(
                                "node_bandwidth_delete_api",
                                fingerprint=s.subscription.relay.fingerprint,
                            ),
                        )
                    ),
                )
                for s in subscriptions
            ],
        )

    @staticmethod
    def get_subscription(email: str, fingerprint: str) -> NodeBandwidthSubDTO:
        """Get subscription for user with fingerprint

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the Node

        Returns:
            NodeBandwidthSub: Node Bandwidth Subscription
        """
        subscription = NodeBandwidthDAO.get_subscription_for_user_fp(email, fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        return NodeBandwidthSubDTO(
            fingerprint=fingerprint,
            wait_for=subscription.wait_for,
            threshold=subscription.threshold,
        )

    @staticmethod
    def create(email: str, node: NodeBandwidthSubDTO) -> RedirectResponseDTO:
        """Create a subscription

        Args:
            email (str): Email of the user
            node (InsertNodeBandwidthSubDTO): Data for subscription

        Returns:
            RedirectResponseDTO: Create subscription response
        """
        relay = RelayDAO.get_relay_with_fp(fingerprint=node.fingerprint)
        if not relay:
            return RedirectResponseDTO(
                notification=SnackbarMessage.RELAY_NOT_FOUND.value,
                redirect_url=url_for("node_bandwidth_list"),
            )

        subscription = SubscriptionDAO.get_subscription_for_user_fp(email, node.fingerprint)
        if not subscription:
            SubscriptionDAO.create(InsertSubscriptionDTO(fingerprint=node.fingerprint, email=email))

        node_bandwidth_sub = NodeBandwidthDAO.get_subscription_for_user_fp(
            email=email, fingerprint=node.fingerprint
        )
        if node_bandwidth_sub:
            return RedirectResponseDTO(
                notification=SnackbarMessage.SUBSCRIPTION_EXISTS.value,
                redirect_url=url_for("node_bandwidth_list"),
            )

        NodeBandwidthDAO.create(email, node)
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_CREATED.value,
            redirect_url=url_for("node_bandwidth_list"),
        )

    @staticmethod
    def modify(email: str, node: NodeBandwidthSubDTO) -> RedirectResponseDTO:
        """Modify a subscription

        Args:
            email (str): Email of the user
            node (InsertNodeBandwidthSubDTO): Data for subscription

        Returns:
            RedirectResponseDTO: Modify subscription response
        """
        subscription = NodeBandwidthDAO.get_subscription_for_user_fp(email, node.fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        subscription.wait_for = node.wait_for
        subscription.threshold = node.threshold
        NodeBandwidthDAO.commit()
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_MODIFIED.value,
            redirect_url=url_for("node_bandwidth_list"),
        )

    @staticmethod
    def enable(email: str, fingerprint: str) -> RedirectResponseDTO:
        """Enable a subscription

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the node
        """
        subscription = NodeBandwidthDAO.get_subscription_for_user_fp(email, fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        subscription.is_active = True
        NodeBandwidthDAO.commit()
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_ENABLED.value,
            redirect_url=url_for("node_bandwidth_list"),
        )

    @staticmethod
    def disable(email: str, fingerprint: str) -> RedirectResponseDTO:
        """Disable a subscription

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the node
        """
        subscription = NodeBandwidthDAO.get_subscription_for_user_fp(email, fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        subscription.is_active = False
        NodeBandwidthDAO.commit()
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_DISABLED.value,
            redirect_url=url_for("node_bandwidth_list"),
        )

    @staticmethod
    def delete(email: str, fingerprint: str) -> RedirectResponseDTO:
        """Delete a subscription

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the node
        """
        subscription = NodeBandwidthDAO.get_subscription_for_user_fp(email, fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        NodeBandwidthDAO.delete(subscription)
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_DELETED.value,
            redirect_url=url_for("node_bandwidth_list"),
        )

    @classmethod
    def send_issue_start_email(
        cls, node: OnionooNodeInterface, subscription: NodeBandwidthSub
    ) -> None:
        """Send email to notify when the issue starts

        Args:
            node (OnionooNodeInterface): Node data from onionoo
            subscription (NodeBandwidthSub): Subscription data
        """
        send_to: str = subscription.subscription.subscriber.email
        NodeBandwidthEmailSvc(
            fingerprint=node.fingerprint,
            threshold=subscription.threshold,
            wait_for=subscription.wait_for,
        ).send_email(send_to)

    @classmethod
    def validate(cls, subscription_id: int, node: OnionooNodeInterface) -> None:
        """Validate a subscription

        Args:
            subscription_id (int): Subscription ID
            node (OnionooNodeInterface): Node Data from onionoo
        """
        subscription = NodeBandwidthDAO.get_by_sub_id(subscription_id)

        if not subscription:
            return
        if not subscription.is_active:
            return
        if not node.advertised_bandwidth:
            return

        if not subscription.issue_first_seen:
            # Relay was previously running fine
            if node.advertised_bandwidth < subscription.threshold:
                # Relay was running fine & is now problemetic
                subscription.issue_first_seen = datetime.utcnow()
        else:
            # Relay was previously having issues
            if node.advertised_bandwidth >= subscription.threshold:
                # Relay is now running fine
                subscription.issue_first_seen = None
                subscription.emailed = False
            else:
                # Relay is still having issues
                elapsed_time = time_diff(subscription.issue_first_seen, datetime.utcnow())
                if elapsed_time >= subscription.wait_for:
                    # Relay is problemetic for more than the waiting time
                    if not subscription.emailed:
                        cls.send_issue_start_email(node, subscription)
                        subscription.emailed = True
        NodeBandwidthDAO.commit()
