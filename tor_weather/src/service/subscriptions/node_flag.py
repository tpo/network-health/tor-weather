from datetime import datetime
from typing import Any, List

from flask import url_for

from tor_weather.error import BadRequest
from tor_weather.src.constants.common import SnackbarMessage
from tor_weather.src.dao.relay import RelayDAO
from tor_weather.src.dao.subscription import SubscriptionDAO
from tor_weather.src.dao.subscriptions.node_flag.exit_sub import NodeFlagExitDAO
from tor_weather.src.dao.subscriptions.node_flag.fast_sub import NodeFlagFastDAO
from tor_weather.src.dao.subscriptions.node_flag.guard_sub import NodeFlagGuardDAO
from tor_weather.src.dao.subscriptions.node_flag.stable_sub import NodeFlagStableDAO
from tor_weather.src.dto.common import Flag
from tor_weather.src.dto.onionoo import OnionooNodeInterface
from tor_weather.src.dto.presentation.table import (
    ITable,
    ITableHeader,
    ITableIconCell,
    ITableIconCellObject,
    ITableTextCell,
)
from tor_weather.src.dto.response import RedirectResponseDTO
from tor_weather.src.dto.subcriptions.node_flag import INodeFlagTableRow, NodeFlagSubDTO
from tor_weather.src.dto.subscription import InsertSubscriptionDTO
from tor_weather.src.service.email.node_flag import NodeFlagEmailSvc
from tor_weather.src.service.subscriptions import SubscriptionBaseSvc
from tor_weather.utilities import time_diff


class NodeFlagSubscriptionSvc(SubscriptionBaseSvc):
    @staticmethod
    def get_dao_for_flag(flag: str) -> Any:
        """Get DAO object for the flag

        Args:
            flag (str): Flag for subscription

        Raises:
            BadRequest: Exception incase flag is unexpected

        Returns:
            Any: DAO class
        """
        if flag == Flag.Exit.value:
            return NodeFlagExitDAO
        if flag == Flag.Fast.value:
            return NodeFlagFastDAO
        if flag == Flag.Guard.value:
            return NodeFlagGuardDAO
        if flag == Flag.Stable.value:
            return NodeFlagStableDAO
        else:
            raise BadRequest(f"Subscriptions not available for flag - {flag}")

    @classmethod
    def get_subscriptions_for_flag(cls, email: str, flag: Flag) -> List[INodeFlagTableRow]:
        subscriptions = cls.get_dao_for_flag(flag.value).get_subs_for_user(email)
        return [
            INodeFlagTableRow(
                fingerprint=ITableTextCell(text=s.subscription.relay.fingerprint),
                wait_for=ITableTextCell(text=f"{s.wait_for} hrs"),
                flag=ITableTextCell(text=flag.name),
                is_active=ITableIconCell(
                    icon=(
                        ITableIconCellObject(
                            name="check_box",
                            type="filled",
                            url=url_for(
                                "node_flag_disable_api",
                                fingerprint=s.subscription.relay.fingerprint,
                                flag=flag.value,
                            ),
                        )
                        if s.is_active
                        else ITableIconCellObject(
                            name="check_box_outline_blank",
                            type="outlined",
                            url=url_for(
                                "node_flag_enable_api",
                                fingerprint=s.subscription.relay.fingerprint,
                                flag=flag.value,
                            ),
                        )
                    )
                ),
                edit=ITableIconCell(
                    icon=ITableIconCellObject(
                        name="edit",
                        type="outlined",
                        url=url_for(
                            "node_flag_modify",
                            fingerprint=s.subscription.relay.fingerprint,
                            flag=flag.value,
                        ),
                    )
                ),
                delete=ITableIconCell(
                    icon=ITableIconCellObject(
                        name="delete",
                        type="outlined",
                        url=url_for(
                            "node_flag_delete_api",
                            fingerprint=s.subscription.relay.fingerprint,
                            flag=flag.value,
                        ),
                    )
                ),
            )
            for s in subscriptions
        ]

    @classmethod
    def get_subscriptions(cls, email: str) -> ITable:
        """Get subscriptions for a user

        Args:
            email (str): Email of the user
            flag (str): Flag for subscription

        Returns:
            List[UINodeFlagListItem]: List of subscriptions
        """
        return ITable(
            headers=[
                ITableHeader(label="Fingerprint", value="fingerprint"),
                ITableHeader(label="Wait For", value="wait_for"),
                ITableHeader(label="Flag", value="flag"),
                ITableHeader(label="Active", value="is_active"),
                ITableHeader(label="", value="edit"),
                ITableHeader(label="", value="delete"),
            ],
            content=[
                *cls.get_subscriptions_for_flag(email=email, flag=Flag.Exit),
                *cls.get_subscriptions_for_flag(email=email, flag=Flag.Fast),
                *cls.get_subscriptions_for_flag(email=email, flag=Flag.Guard),
                *cls.get_subscriptions_for_flag(email=email, flag=Flag.Stable),
            ],
        )

    @classmethod
    def get_subscription(cls, email: str, fingerprint: str, flag: str) -> NodeFlagSubDTO:
        """Get subscription for user with fingerprint

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the Node

        Returns:
            NodeFlagSubDTO: Node Flag Subscription
        """
        dao = cls.get_dao_for_flag(flag)
        subscription = dao.get_subscription_for_user_fp(email, fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        return NodeFlagSubDTO(fingerprint=fingerprint, wait_for=subscription.wait_for, flag=flag)

    @classmethod
    def create(cls, email: str, node: NodeFlagSubDTO) -> RedirectResponseDTO:
        """Create a subscription

        Args:
            email (str): Email of the user
            node (InsertNodeFlagSubDTO): Data for subscription

        Returns:
            ResponseDTO: Create subscription response
        """
        dao = cls.get_dao_for_flag(flag=node.flag)
        relay = RelayDAO.get_relay_with_fp(fingerprint=node.fingerprint)
        if not relay:
            return RedirectResponseDTO(
                notification=SnackbarMessage.RELAY_NOT_FOUND.value,
                redirect_url=url_for("node_flag_list"),
            )

        subscription = SubscriptionDAO.get_subscription_for_user_fp(email, node.fingerprint)
        if not subscription:
            SubscriptionDAO.create(InsertSubscriptionDTO(fingerprint=node.fingerprint, email=email))

        node_flag_sub = dao.get_subscription_for_user_fp(email=email, fingerprint=node.fingerprint)
        if node_flag_sub:
            return RedirectResponseDTO(
                notification=SnackbarMessage.SUBSCRIPTION_EXISTS.value,
                redirect_url=url_for("node_flag_list"),
            )

        dao.create(email, node)
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_CREATED.value,
            redirect_url=url_for("node_flag_list"),
        )

    @classmethod
    def modify(cls, email: str, node: NodeFlagSubDTO) -> RedirectResponseDTO:
        """Modify a subscription

        Args:
            email (str): Email of the user
            node (InsertNodeFlagSubDTO): Data for subscription

        Returns:
            RedirectResponseDTO: Modify subscription response
        """
        dao = cls.get_dao_for_flag(node.flag)
        subscription = dao.get_subscription_for_user_fp(email, node.fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        subscription.wait_for = node.wait_for
        dao.commit()
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_MODIFIED.value,
            redirect_url=url_for("node_flag_list"),
        )

    @classmethod
    def enable(cls, email: str, fingerprint: str, flag: str) -> RedirectResponseDTO:
        """Enable a subscription

        Args:
            email (str): Email of the user
            flag (str): Flag for subscription
            fingerprint (str): Fingerprint of the node
        """
        dao = cls.get_dao_for_flag(flag)
        subscription = dao.get_subscription_for_user_fp(email, fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        subscription.is_active = True
        dao.commit()
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_ENABLED.value,
            redirect_url=url_for("node_flag_list"),
        )

    @classmethod
    def disable(cls, email: str, fingerprint: str, flag: str) -> RedirectResponseDTO:
        """Disable a subscription

        Args:
            email (str): Email of the user
            flag (str): Flag for subscription
            fingerprint (str): Fingerprint of the node
        """
        dao = cls.get_dao_for_flag(flag)
        subscription = dao.get_subscription_for_user_fp(email, fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        subscription.is_active = False
        dao.commit()
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_DISABLED.value,
            redirect_url=url_for("node_flag_list"),
        )

    @classmethod
    def delete(cls, email: str, fingerprint: str, flag: str) -> RedirectResponseDTO:
        """Delete a subscription

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the node
        """
        dao = cls.get_dao_for_flag(flag)
        subscription = dao.get_subscription_for_user_fp(email, fingerprint)
        if not subscription:
            raise BadRequest("Requested Subscription does not exit")
        dao.delete(subscription)
        return RedirectResponseDTO(
            notification=SnackbarMessage.SUBSCRIPTION_DELETED.value,
            redirect_url=url_for("node_flag_list"),
        )

    @classmethod
    def send_issue_start_email(
        cls, node: OnionooNodeInterface, subscription: Any, flag: Flag
    ) -> None:
        """Send email to notify when the issue starts

        Args:
            relay (OnionooRelayInterface): Node data from onionoo
            subscription (Any): Subscription data
            flag (str): Flag for which to send the email
        """
        send_to: str = subscription.subscription.subscriber.email
        NodeFlagEmailSvc(
            fingerprint=node.fingerprint, wait_for=subscription.wait_for, flag=flag
        ).send_email(send_to)

    @classmethod
    def validate(cls, subscription_id: int, node: OnionooNodeInterface) -> None:
        """Validate a subscription

        Args:
            subscription_id (int): Subscription ID
            node (OnionooNodeInterface): Node Data from onionoo
        """
        cls.validate_flag(subscription_id, node, Flag.Exit)
        cls.validate_flag(subscription_id, node, Flag.Stable)
        cls.validate_flag(subscription_id, node, Flag.Fast)
        cls.validate_flag(subscription_id, node, Flag.Guard)

    @classmethod
    def validate_flag(cls, subscription_id: int, node: OnionooNodeInterface, flag: Flag) -> None:
        """Validate a subscription for flag

        Args:
            subscription_id (int): Subscription ID
            node (OnionooNodeInterface): Node Data from onionoo
            flag (str): Flag for subscription
        """
        dao = cls.get_dao_for_flag(flag.value)
        subscription = dao.get_by_sub_id(subscription_id)

        if not subscription:
            return
        if not subscription.is_active:
            return
        if not node.flags:
            return

        if not subscription.issue_first_seen:
            # Relay was previously running fine
            if flag.value not in node.flags:
                # Relay was running fine & is now problemetic
                subscription.issue_first_seen = datetime.utcnow()
        else:
            # Relay was previously having issues
            if flag.value in node.flags:
                # Relay is now running fine
                subscription.issue_first_seen = None
                subscription.emailed = False
            else:
                # Relay is still having issues
                elapsed_time = time_diff(subscription.issue_first_seen, datetime.utcnow())
                if elapsed_time >= subscription.wait_for:
                    # Relay is problemetic for more than the waiting time
                    if not subscription.emailed:
                        cls.send_issue_start_email(node, subscription, flag)
                        subscription.emailed = True
        dao.commit()
