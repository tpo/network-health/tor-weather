from flask import current_app, render_template

from tor_weather.src.service.email.base import BaseEmailSvc


class ThankYouEmailSvc(BaseEmailSvc):
    template = "thank_you.html"

    def __init__(self) -> None:
        super().__init__()

    def get_subject(self) -> str:
        return "Thank you for running a Tor node!"

    def get_text_body(self) -> str:
        return ""

    def get_html_body(self) -> str:
        return render_template(f"mail/{self.template}", website=current_app.config["BASE_URL"])
