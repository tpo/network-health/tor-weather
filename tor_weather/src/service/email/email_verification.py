from flask import current_app, render_template

from tor_weather.src.service.email.base import BaseEmailSvc


class VerificationEmailSvc(BaseEmailSvc):
    def __init__(self, verification_code: str) -> None:
        self.verification_code = verification_code
        super().__init__()

    def get_subject(self) -> str:
        return "Tor-Weather : Verify your email address"

    def get_html_body(self) -> str:
        return render_template(
            "mail/email_verification.html", verification_url=self.get_verification_url()
        )

    def get_verification_url(self) -> str:
        base_url = current_app.config["BASE_URL"]
        return f"{base_url}/api/verify?code={self.verification_code}"

    def get_text_body(self) -> str:
        return f"""Hi there,

Welcome to the Tor-Weather Service.

Congratulations, you have successfully created your account! We just need to verify your email to \
finish account registration. Click on the link below to verify your account:

{self.get_verification_url()}

Thank You!
        """
