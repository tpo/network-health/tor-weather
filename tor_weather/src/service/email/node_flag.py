from flask import render_template

from tor_weather.src.dto.common import Flag
from tor_weather.src.service.email.base import BaseEmailSvc


class NodeFlagEmailSvc(BaseEmailSvc):
    template = "node_flag.html"

    def __init__(self, fingerprint: str, flag: Flag, wait_for: int) -> None:
        self.fingerprint = fingerprint
        self.flag = flag
        self.wait_for = wait_for
        super().__init__()

    def get_subject(self) -> str:
        return "Tor-Weather : Node-Flag Alert"

    def get_html_body(self) -> str:
        return render_template(
            f"mail/{self.template}",
            fingerprint=self.fingerprint,
            wait_for=self.wait_for,
            flag=self.flag.name,
            metrics_url=self.get_metrics_url(self.fingerprint),
        )

    def get_metrics_url(self, fingerprint: str) -> str:
        return f"https://metrics.torproject.org/rs.html#details/{fingerprint}"

    def get_text_body(self) -> str:
        return f"""Hi there,

Our automated system has detected that your node has lost a flag. Immediate action is recommended to address
this issue, restore service, and ensure continued efficiency and reliability.

Node Fingerprint: {self.fingerprint}
Flag Lost: {self.flag.name}
Duration Since Last Flag Assignment: {self.wait_for} hour(s)

Metrics URL: {self.get_metrics_url(self.fingerprint)}

During this extended period, the loss of one of your node's flags may impact the performance of your Tor node \
service.

Thank You!
        """
