from flask import render_template

from tor_weather.src.service.email.base import BaseEmailSvc


class NodeBandwidthEmailSvc(BaseEmailSvc):
    template = "node_bandwidth.html"

    def __init__(self, fingerprint: str, threshold: int, wait_for: int) -> None:
        self.fingerprint = fingerprint
        self.threshold = threshold
        self.wait_for = wait_for
        super().__init__()

    def get_subject(self) -> str:
        return "Tor-Weather : Node-Bandwidth Alert"

    def get_html_body(self) -> str:
        return render_template(
            f"mail/{self.template}",
            fingerprint=self.fingerprint,
            threshold=self.threshold,
            wait_for=self.wait_for,
            metrics_url=self.get_metrics_url(self.fingerprint),
        )

    def get_metrics_url(self, fingerprint: str) -> str:
        return f"https://metrics.torproject.org/rs.html#details/{fingerprint}"

    def get_text_body(self) -> str:
        return f"""Hi there,

Our automated system has detected a sustained bandwidth drop on your relay node. Immediate action is recommended to \
ensure continued efficiency and reliability.

Node Fingerprint: {self.fingerprint}
Bandwidth Threshold: {self.threshold} kBps
Duration Below Threshold: {self.wait_for} hour(s)

Metrics URL: {self.get_metrics_url(self.fingerprint)}

This sustained decrease could impact the performance of your Tor relay service. We strongly recommend investigating \
and addressing this issue promptly to ensure the continued efficiency and reliability of your relay node.

Thank You!
        """
