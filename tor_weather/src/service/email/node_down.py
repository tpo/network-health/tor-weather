from flask import render_template

from tor_weather.src.service.email.base import BaseEmailSvc


class NodeDownEmailSvc(BaseEmailSvc):
    template = "node_down.html"

    def __init__(self, fingerprint: str, wait_for: int) -> None:
        self.fingerprint = fingerprint
        self.wait_for = wait_for
        super().__init__()

    def get_subject(self) -> str:
        return "Tor-Weather : Node-Down Alert"

    def get_html_body(self) -> str:
        return render_template(
            f"mail/{self.template}",
            fingerprint=self.fingerprint,
            wait_for=self.wait_for,
            metrics_url=self.get_metrics_url(self.fingerprint),
        )

    def get_metrics_url(self, fingerprint: str) -> str:
        return f"https://metrics.torproject.org/rs.html#details/{fingerprint}"

    def get_text_body(self) -> str:
        return f"""Hi there,

Our automated system has detected that your relay node is currently offline. Immediate action is recommended to \
restore service and ensure continued efficiency and reliability.

Node Fingerprint: {self.fingerprint}
Duration Since Last Connection: {self.wait_for} hour(s)

Metrics URL: {self.get_metrics_url(self.fingerprint)}

This prolonged period of inactivity may impact the performance of your Tor relay service. We strongly recommend \
investigating and addressing this issue promptly to ensure the continued efficiency and reliability of your relay \
node.

Thank You!
        """
