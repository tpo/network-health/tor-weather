from abc import ABC, abstractmethod
from dataclasses import asdict
from textwrap import dedent

from flask import current_app

from tor_weather.src.dto.mail import MailDTO
from tor_weather.tasks.send_mail import send_mail


class BaseEmailSvc(ABC):
    @abstractmethod
    def get_subject(self) -> str:
        """Get Subject for the email

        Returns:
            str: Subject for the email
        """
        pass

    @abstractmethod
    def get_html_body(self) -> str:
        """Get HTML Body for the email

        Returns:
            str: HTML Body for the email
        """
        pass

    @abstractmethod
    def get_text_body(self) -> str:
        """Get Text Body for the email

        Returns:
            str: Text Body for the email
        """
        pass

    def get_mail_dto(self, to: str) -> MailDTO:
        """Create email dto for queuing email

        Args:
            to (str): Email-Id of the recipient

        Returns:
            MailDTO: Mail DTO object
        """
        return MailDTO(
            from_email=current_app.config["MAIL_USERNAME"],
            to=[to],
            subject=self.get_subject(),
            html_body=self.get_html_body(),
            text_body=dedent(self.get_text_body()),
        )

    def send_email(self, to: str) -> None:
        """Send email

        Args:
            to (str): Email-Id of the recipient
        """
        content = self.get_mail_dto(to)
        send_mail.delay(asdict(content))  # type: ignore
