from flask import render_template

from tor_weather.src.service.email.base import BaseEmailSvc


class WelcomeEmailSvc(BaseEmailSvc):
    template = "welcome.html"

    def __init__(self) -> None:
        super().__init__()

    def get_subject(self) -> str:
        return "Welcome to Tor-Weather!"

    def get_text_body(self) -> str:
        return ""

    def get_html_body(self) -> str:
        return render_template(f"mail/{self.template}")
