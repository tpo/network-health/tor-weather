from flask import url_for
from flask_login import login_user, logout_user

from tor_weather.error import BadRequest
from tor_weather.src.constants.common import SnackbarMessage
from tor_weather.src.dao.subscriber import SubscriberDAO
from tor_weather.src.dto.response import RedirectResponseDTO
from tor_weather.src.service.email.email_verification import VerificationEmailSvc
from tor_weather.utilities.encryption import decrypt, encrypt
from tor_weather.utilities.hash import check_password, hash_password


class SubscriberSvc:
    @staticmethod
    def _decrypt_email_verification_code(code: str) -> str:
        """Decrypt email verification code to get user email

        Args:
            code (str): Email verification code

        Returns:
            str: Email of the user
        """
        return decrypt(code)

    @staticmethod
    def _generate_email_verification_code(email: str) -> str:
        """Generate email verification code

        Args:
            email (str): Email of the user

        Returns:
            str: Email verification code
        """
        email_verification_code = encrypt(email)
        return email_verification_code

    @classmethod
    def _send_account_verification_email(cls, email: str) -> None:
        """Sends an email with the verification code for the signup

        Args:
            email (str): Email of the user to send the verification email to.
        """
        verification_code = cls._generate_email_verification_code(email)
        VerificationEmailSvc(verification_code).send_email(email)

    @staticmethod
    def logout() -> RedirectResponseDTO:
        """Logs out the user from the dashboard

        Returns:
            RedirectResponseDTO: Logout Response
        """
        logout_user()
        return RedirectResponseDTO(redirect_url=url_for("login"))

    @classmethod
    def login(cls, email: str, password: str) -> RedirectResponseDTO:
        """Logs in the user to the dashboard

        Args:
            email (str): Email of the user
            password (str): Password of the user

        Returns:
            RedirectResponseDTO: Login Response
        """
        user = SubscriberDAO.get_user_by_email(email)

        if not user:
            return RedirectResponseDTO(
                notification=SnackbarMessage.USER_NOT_FOUND.value,
                redirect_url=url_for("register"),
            )
        if not check_password(password, user.password):
            return RedirectResponseDTO(
                notification=SnackbarMessage.INCORRECT_CREDENTIALS.value,
                redirect_url=url_for("login"),
            )
        if not user.is_confirmed:
            cls._send_account_verification_email(email)
            return RedirectResponseDTO(
                notification=SnackbarMessage.VERIFICATION_EMAIL_RESEND.value,
                redirect_url=url_for("login"),
            )

        login_user(user)
        return RedirectResponseDTO(redirect_url=url_for("dashboard_home"))

    @classmethod
    def register(cls, email: str, password: str) -> RedirectResponseDTO:
        """Register a new user to the dashboard

        Args:
            email (str): Email of the user
            password (str): Password of the user

        Returns:
            RedirectResponseDTO: Register Response
        """
        user = SubscriberDAO.get_user_by_email(email)

        if user:
            return RedirectResponseDTO(
                notification=SnackbarMessage.USER_ALREADY_EXISTS.value,
                redirect_url=url_for("login"),
            )

        SubscriberDAO.create(email=email, password=hash_password(password))
        cls._send_account_verification_email(email)
        return RedirectResponseDTO(
            notification=SnackbarMessage.VERIFICATION_EMAIL_SENT.value,
            redirect_url=url_for("login"),
        )

    @classmethod
    def verify(cls, verification_code: str) -> RedirectResponseDTO:
        """Verify a user account

        Args:
            verification_code (str): Verification code

        Raises:
            BadRequest: Incorrect email-verification code

        Returns:
            RedirectResponseDTO: Verification Response
        """
        email = cls._decrypt_email_verification_code(verification_code)
        user = SubscriberDAO.get_user_by_email(email)

        if not user:
            raise BadRequest("Incorrect verification-code")
        if user.is_confirmed:
            return RedirectResponseDTO(
                notification=SnackbarMessage.EMAIL_VERIFICATION_REPEAT.value,
                redirect_url=url_for("login"),
            )

        SubscriberDAO.verify(email)
        return RedirectResponseDTO(
            notification=SnackbarMessage.EMAIL_VERIFICATION_SUCCESS.value,
            redirect_url=url_for("login"),
        )
