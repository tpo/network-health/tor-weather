from tor_weather.src.adapter.relay_adapter import RelayAdapter
from tor_weather.src.constants.common import ISubscription, ISubscriptionCategory
from tor_weather.src.dao.relay import RelayDAO
from tor_weather.src.dao.subscription import SubscriptionDAO
from tor_weather.src.dto.onionoo import OnionooNodeInterface
from tor_weather.src.dto.relay import InsertRelayDTO
from tor_weather.src.service.subscriptions.node_bandwidth import NodeBandwidthSubscriptionSvc
from tor_weather.src.service.subscriptions.node_down import NodeDownSubscriptionSvc
from tor_weather.src.service.subscriptions.node_flag import NodeFlagSubscriptionSvc


class SubscriptionSvc:
    @staticmethod
    def _get_subscription_map() -> dict[str, list[str]]:
        return {
            ISubscriptionCategory.NODE_STATUS.value: [
                ISubscription.NODE_DOWN.value,
                ISubscription.NODE_BANDWIDTH.value,
            ],
            ISubscriptionCategory.NODE_FLAG.value: [
                ISubscription.NODE_FLAG_EXIT.value,
                ISubscription.NODE_FLAG_FAST.value,
                ISubscription.NODE_FLAG_GUARD.value,
                ISubscription.NODE_FLAG_STABLE.value,
            ],
        }

    @staticmethod
    def insert_relay_if_not_exists(relay_dto: InsertRelayDTO) -> None:
        if not RelayDAO.get_relay_with_fp(relay_dto.fingerprint):
            RelayDAO.create(relay_dto)

    @classmethod
    def validate_node(cls, node: OnionooNodeInterface, is_bridge=False) -> None:
        relay_dto = RelayAdapter.onionoo_to_dto(node, is_bridge)
        cls.insert_relay_if_not_exists(relay_dto)
        subscriptions = SubscriptionDAO.get_subscriptions_for_fp(node.fingerprint)
        for subscription in subscriptions:
            NodeDownSubscriptionSvc.validate(subscription.id, node)
            NodeBandwidthSubscriptionSvc.validate(subscription.id, node)
            NodeFlagSubscriptionSvc.validate(subscription.id, node)
