from typing import Optional

from flask import url_for

from tor_weather.src.constants.form import (
    FORM_BUTTON,
    FORM_FIELD,
    FORM_MAP,
    FormDTO,
    FormFieldDTO,
    SelectFormFieldDTO,
)
from tor_weather.src.dto.subcriptions.node_bandwidth import NodeBandwidthSubDTO
from tor_weather.src.dto.subcriptions.node_down import NodeDownSubDTO
from tor_weather.src.dto.subcriptions.node_flag import NodeFlagSubDTO


class FormSvc:
    @staticmethod
    def construct_form_field(
        field: FORM_FIELD, disabled: bool = False, default: Optional[str | int] = None
    ) -> FormFieldDTO:
        form_field = FORM_MAP[field]
        return FormFieldDTO(
            value=form_field.value,
            input_type=form_field.field_type,
            display_name=form_field.label,
            disabled=disabled,
            default_value=default if default else "",
            options=form_field.options if isinstance(form_field, SelectFormFieldDTO) else None,
        )

    @classmethod
    def get_node_bandwidth_create_form(cls) -> FormDTO:
        return FormDTO(
            header="Create  - Node Bandwidth Subscription",
            button=FORM_BUTTON.CREATE.value,
            endpoint=url_for("node_bandwidth_create_api"),
            fields=[
                cls.construct_form_field(field=FORM_FIELD.FINGERPRINT),
                cls.construct_form_field(field=FORM_FIELD.THRESHOLD_BANDWIDTH),
                cls.construct_form_field(field=FORM_FIELD.WAIT_FOR),
            ],
        )

    @classmethod
    def get_node_bandwidth_modify_form(cls, default: NodeBandwidthSubDTO) -> FormDTO:
        return FormDTO(
            header="Modify  - Node Bandwidth Subscription",
            button=FORM_BUTTON.MODIFY.value,
            endpoint=url_for("node_bandwidth_modify_api"),
            fields=[
                cls.construct_form_field(
                    field=FORM_FIELD.FINGERPRINT,
                    disabled=True,
                    default=default.fingerprint,
                ),
                cls.construct_form_field(
                    field=FORM_FIELD.THRESHOLD_BANDWIDTH,
                    default=default.threshold,
                ),
                cls.construct_form_field(
                    field=FORM_FIELD.WAIT_FOR,
                    default=default.wait_for,
                ),
            ],
        )

    @classmethod
    def get_node_down_create_form(cls) -> FormDTO:
        return FormDTO(
            header="Create  - Node Down Subscription",
            button=FORM_BUTTON.CREATE.value,
            endpoint=url_for("node_down_create_api"),
            fields=[
                cls.construct_form_field(field=FORM_FIELD.FINGERPRINT),
                cls.construct_form_field(field=FORM_FIELD.WAIT_FOR),
            ],
        )

    @classmethod
    def get_node_down_modify_form(cls, default: NodeDownSubDTO) -> FormDTO:
        return FormDTO(
            header="Modify  - Node Down Subscription",
            button=FORM_BUTTON.MODIFY.value,
            endpoint=url_for("node_down_modify_api"),
            fields=[
                cls.construct_form_field(
                    field=FORM_FIELD.FINGERPRINT,
                    disabled=True,
                    default=default.fingerprint,
                ),
                cls.construct_form_field(
                    field=FORM_FIELD.WAIT_FOR,
                    default=default.wait_for,
                ),
            ],
        )

    @classmethod
    def get_node_flag_create_form(cls) -> FormDTO:
        return FormDTO(
            header="Create  - Node Flag Subscription",
            button=FORM_BUTTON.CREATE.value,
            endpoint=url_for("node_flag_create_api"),
            fields=[
                cls.construct_form_field(field=FORM_FIELD.FINGERPRINT),
                cls.construct_form_field(field=FORM_FIELD.FLAG),
                cls.construct_form_field(field=FORM_FIELD.WAIT_FOR),
            ],
        )

    @classmethod
    def get_node_flag_modify_form(cls, default: NodeFlagSubDTO) -> FormDTO:
        return FormDTO(
            header="Modify  - Node Flag Subscription",
            button=FORM_BUTTON.MODIFY.value,
            endpoint=url_for("node_flag_modify_api"),
            fields=[
                cls.construct_form_field(
                    field=FORM_FIELD.FINGERPRINT,
                    disabled=True,
                    default=default.fingerprint,
                ),
                cls.construct_form_field(
                    field=FORM_FIELD.FLAG,
                    disabled=True,
                    default=default.flag,
                ),
                cls.construct_form_field(
                    field=FORM_FIELD.WAIT_FOR,
                    default=default.wait_for,
                ),
            ],
        )
