from typing import List

from tor_weather.src.constants.info_card import INFO_CARD_DATA, INFO_CARD_KEY, InfoCardDTO


class InfoCardSvc:
    @staticmethod
    def get_cards(keys: List[INFO_CARD_KEY]) -> List[InfoCardDTO]:
        """Get cards for keys

        Args:
            keys (List[INFO_CARD_KEY]): List of keys

        Returns:
            List[InfoCardDTO]: List of Cards
        """
        return [INFO_CARD_DATA[key.value] for key in keys]

    @classmethod
    def get_node_bandwidth_cards(cls) -> List[InfoCardDTO]:
        """Get info card for node-bandwidth

        Returns:
            List[InfoCardDTO]: List of Cards
        """
        keys = [INFO_CARD_KEY.FINGERPRINT, INFO_CARD_KEY.THRESHOLD, INFO_CARD_KEY.WAIT_FOR]
        return cls.get_cards(keys)

    @classmethod
    def get_node_down_cards(cls) -> List[InfoCardDTO]:
        """Get info card for node-down

        Returns:
            List[InfoCardDTO]: List of Cards
        """
        keys = [INFO_CARD_KEY.FINGERPRINT, INFO_CARD_KEY.WAIT_FOR]
        return cls.get_cards(keys)

    @classmethod
    def get_node_flag_cards(cls) -> List[InfoCardDTO]:
        """Get info card for node-flag

        Returns:
            List[InfoCardDTO]: List of Cards
        """
        keys = [INFO_CARD_KEY.FINGERPRINT, INFO_CARD_KEY.FLAG, INFO_CARD_KEY.WAIT_FOR]
        return cls.get_cards(keys)
