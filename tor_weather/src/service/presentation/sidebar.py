from typing import List

from tor_weather.src.constants.sidebar import (
    PAGE,
    PAGE_MAP,
    SIDEBAR_CATEGORY,
    SidebarCategoryDTO,
    SidebarDTO,
    SidebarEntryDTO,
)


class SidebarSvc:
    @staticmethod
    def get_base_data(active_page: PAGE) -> dict[SIDEBAR_CATEGORY, List[SidebarEntryDTO]]:
        """Get internal sidebase data representation for active page

        Args:
            active_page (PAGE): Active Page

        Returns:
            dict[SIDEBAR_CATEGORY, List[SidebarEntryDTO]]: Internal data representation
        """
        sidebar_map: dict[SIDEBAR_CATEGORY, List[SidebarEntryDTO]] = {}

        for page_name, page_data in PAGE_MAP.items():
            if not sidebar_map.get(page_data.category):
                sidebar_map[page_data.category] = []

            sidebar_map[page_data.category].append(
                SidebarEntryDTO(
                    display_name=page_data.display_name,
                    url=page_data.url,
                    icon=page_data.icon,
                    is_active=active_page == page_name,
                )
            )

        return sidebar_map

    @staticmethod
    def translate_data(data: dict[SIDEBAR_CATEGORY, List[SidebarEntryDTO]]) -> SidebarDTO:
        """Translate sidebar data for presentation layer

        Args:
            data (dict[SIDEBAR_CATEGORY, List[SidebarEntryDTO]]): Internal data representation

        Returns:
            SidebarDTO: Sidebar data
        """
        return SidebarDTO(
            categories=[
                SidebarCategoryDTO(display_name=category.value, options=data[category])
                for category in data.keys()
            ]
        )

    @classmethod
    def get_data(cls, active_page: PAGE) -> SidebarDTO:
        """Get sidebar data for active page

        Args:
            active_page (PAGE): Active Page

        Returns:
            SidebarDTO: Sidebar Data
        """
        base_data = cls.get_base_data(active_page)
        return cls.translate_data(base_data)
