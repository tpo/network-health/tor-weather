from typing import List

from tor_weather.src.constants.breadcrumb import BREADCRUMB_LEVEL
from tor_weather.src.constants.sidebar import PAGE, PAGE_MAP, SIDEBAR_CATEGORY


class BreadcrumbSvc:
    @staticmethod
    def get_category(page: PAGE) -> SIDEBAR_CATEGORY:
        """Get category for the page

        Args:
            page (PAGE): Current page

        Returns:
            SIDEBAR_CATEGORY: Category for the page
        """
        return PAGE_MAP[page].category

    @classmethod
    def get_data(cls, page: PAGE, extended_level: List[BREADCRUMB_LEVEL] = []) -> List[str]:
        """Get breadcrumb data for the page

        Args:
            page (PAGE): Current page
            extended_level (List[BREADCRUMB_LEVEL]): Extended breadcrumb levels

        Returns:
            List[str]: Breadcrumb Data
        """
        category = cls.get_category(page)
        extension_list = [level.value for level in extended_level]
        result = [category.value, page.value]
        result.extend(extension_list)
        return result
