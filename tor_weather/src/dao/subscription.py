from typing import List, Optional

from sqlalchemy import exc

from tor_weather.database.relay import Relay
from tor_weather.database.subscriber import Subscriber
from tor_weather.database.subscription import Subscription
from tor_weather.error import BadRequest, InternalError, UnAuthorized
from tor_weather.extensions import db
from tor_weather.src.dto.subscription import InsertSubscriptionDTO


class SubscriptionDAO:
    @staticmethod
    def get_subscription_for_user_fp(email: str, fingerprint: str) -> Optional[Subscription]:
        """Get subscription for user with fingerprint

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the node

        Returns:
            Optional[Subscription]: Subscription
        """
        return Subscription.query.filter(
            Subscription.subscriber.has(Subscriber.email == email),  # type: ignore
            Subscription.relay.has(Relay.fingerprint == fingerprint),  # type: ignore
        ).first()

    @staticmethod
    def get_subscriptions_for_fp(fingerprint: str) -> List[Subscription]:
        """Get subscriptions for node

        Args:
            fingerprint (str): Fingerprint of the node

        Returns:
            List[Subscription]: List of subscriptions
        """
        return Subscription.query.filter(
            Subscription.relay.has(Relay.fingerprint == fingerprint),  # type: ignore
        ).all()

    @staticmethod
    def get_subscriptions_for_user(email: str) -> List[Subscription]:
        """Get subscriptions for user

        Args:
            email (str): Email of the user

        Returns:
            List[Subscription]: List of subscriptions
        """
        return Subscription.query.filter(
            Subscription.subscriber.has(Subscriber.email == email)  # type: ignore
        ).all()

    @staticmethod
    def create(sub: InsertSubscriptionDTO) -> None:
        """Create a subscription

        Args:
            sub (InsertSubscriptionDTO): Subscription data

        Raises:
            UnAuthorized: Subscriber with email does not exist
            BadRequest: Node with fingerprint does not exist
            InternalError: Database error
        """
        subscriber = Subscriber.query.filter_by(email=sub.email).first()
        node = Relay.query.filter_by(fingerprint=sub.fingerprint).first()

        if not subscriber:
            raise UnAuthorized()
        if not node:
            raise BadRequest("Node with the provided fingerprint doesn't exist")

        subscription = Subscription(relay=node, subscriber=subscriber)  # type: ignore
        try:
            db.session.add(subscription)
            db.session.commit()
        except exc.SQLAlchemyError as e:
            db.session.rollback()
            raise InternalError(f"Adding subscription to db failed - {e}")
