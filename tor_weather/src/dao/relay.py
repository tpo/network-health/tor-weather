from dataclasses import asdict
from typing import Optional

from sqlalchemy import exc

from tor_weather.database.relay import Relay
from tor_weather.error import InternalError
from tor_weather.extensions import db
from tor_weather.src.dto.relay import InsertRelayDTO


class RelayDAO:
    @staticmethod
    def get_relay_with_fp(fingerprint: str) -> Optional[Relay]:
        """Get relay with fingerprint

        Args:
            fingerprint (str): Fingerprint of the relay

        Returns:
            Optional[Relay]: Relay object
        """
        return Relay.query.filter_by(fingerprint=fingerprint).first()

    @staticmethod
    def create(relay_dto: InsertRelayDTO) -> None:
        """Insert relay

        Args:
            relay_dto (InsertRelayDTO): Relay DTO

        Raises:
            InternalError: Database error
        """
        relay = Relay(**asdict(relay_dto))
        try:
            db.session.add(relay)
            db.session.commit()
        except exc.SQLAlchemyError as e:
            db.session.rollback()
            raise InternalError(f"Adding relay to db failed - {e}")
