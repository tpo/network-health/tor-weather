from typing import Optional

from sqlalchemy import exc

from tor_weather.database.subscriber import Subscriber
from tor_weather.error import InternalError, UnAuthorized
from tor_weather.extensions import db


class SubscriberDAO:
    @staticmethod
    def get_user_by_email(email: str) -> Optional[Subscriber]:
        """Get user by their email

        Args:
            email (str): Email of the user

        Returns:
            Optional[Subscriber]: Subscriber object to be fetched first
        """
        return Subscriber.query.filter_by(email=email).first()

    @staticmethod
    def create(email: str, password: str) -> None:
        """Insert a new user to the database

        Args:
            email (str): Email of the user
            password (str): Password of the user
        """
        subscriber = Subscriber(email=email, password=password)  # type: ignore
        try:
            db.session.add(subscriber)
            db.session.commit()
        except exc.SQLAlchemyError as e:
            db.session.rollback()
            raise InternalError(f"Adding subscriber to db failed - {e}")

    @classmethod
    def verify(cls, email: str) -> None:
        """Verify a user

        Args:
            email (str): Email of the user

        Raises:
            UnAuthorized: Subscriber with email does not exist
            InternalError: Database error
        """
        user = cls.get_user_by_email(email)
        if not user:
            raise UnAuthorized()

        try:
            user.is_confirmed = True
            db.session.commit()
        except exc.SQLAlchemyError as e:
            db.session.rollback()
            raise InternalError(f"Verifying subscriber failed - {e}")
