from typing import List, Optional

from sqlalchemy import exc

from tor_weather.database.subscriptions.node_flag.stable_sub import NodeFlagStableSub
from tor_weather.error import UnAuthorized
from tor_weather.extensions import app_logger, db
from tor_weather.src.dao.subscriber import SubscriberDAO
from tor_weather.src.dao.subscription import SubscriptionDAO
from tor_weather.src.dto.subcriptions.node_flag import NodeFlagSubDTO


class NodeFlagStableDAO:
    @staticmethod
    def get_by_sub_id(id: int) -> Optional[NodeFlagStableSub]:
        """Get node-flag-stable by subscription id

        Args:
            id (int): Subscription id

        Returns:
            Optional[NodeFlagStableSub]: Node Flag Stable Subscription
        """
        return NodeFlagStableSub.query.filter_by(subscription_id=id).first()

    @classmethod
    def get_subscription_for_user_fp(
        cls, email: str, fingerprint: str
    ) -> Optional[NodeFlagStableSub]:
        """Get node-flag-stable subscription for node for user

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the node

        Raises:
            BadRequest: Bad Request

        Returns:
            Optional[NodeFlagStableSub]: Node Flag Stable Subscription
        """
        user_subs = cls.get_subs_for_user(email)
        sub_with_fp = [s for s in user_subs if s.subscription.relay.fingerprint == fingerprint]
        if len(sub_with_fp) == 0:
            return None
        return sub_with_fp.pop()

    @staticmethod
    def get_subs_for_user(email: str) -> List[NodeFlagStableSub]:
        """Get node-flag-stable subscriptions for user

        Args:
            email (str): Email of the user

        Raises:
            UnAuthorized: Unauthorized access

        Returns:
            List[NodeFlagStableSub]: List of Node Flag Stable Subscription
        """
        user = SubscriberDAO.get_user_by_email(email)

        if not user:
            raise UnAuthorized()

        subscriptions = user.subscriptions
        return [
            subscription.node_flag_stable_sub
            for subscription in subscriptions
            if subscription.node_flag_stable_sub
        ]

    @staticmethod
    def create(email: str, sub: NodeFlagSubDTO) -> None:
        subscription = SubscriptionDAO.get_subscription_for_user_fp(
            email=email, fingerprint=sub.fingerprint
        )
        node_down = NodeFlagStableSub(subscription=subscription, wait_for=sub.wait_for)  # type: ignore
        try:
            db.session.add(node_down)
            db.session.commit()
        except exc.SQLAlchemyError as e:
            app_logger.error(f"Adding node-flag-stable to db failed - {e}")
            db.session.rollback()

    @classmethod
    def delete(cls, subscription: NodeFlagStableSub) -> None:
        db.session.delete(subscription)
        cls.commit()

    @staticmethod
    def commit() -> None:
        db.session.commit()
