from typing import List, Optional

from sqlalchemy import exc

from tor_weather.database.subscriptions.node_bandwidth_sub import NodeBandwidthSub
from tor_weather.error import UnAuthorized
from tor_weather.extensions import app_logger, db
from tor_weather.src.dao.subscriber import SubscriberDAO
from tor_weather.src.dao.subscription import SubscriptionDAO
from tor_weather.src.dto.subcriptions.node_bandwidth import NodeBandwidthSubDTO


class NodeBandwidthDAO:
    @staticmethod
    def get_by_sub_id(id: int) -> Optional[NodeBandwidthSub]:
        """Get node-bandwidth by subscription id

        Args:
            id (int): Subscription id

        Returns:
            Optional[NodeBandwidthSub]: Node Bandwidth Subscription
        """
        return NodeBandwidthSub.query.filter_by(subscription_id=id).first()

    @classmethod
    def get_subscription_for_user_fp(
        cls, email: str, fingerprint: str
    ) -> Optional[NodeBandwidthSub]:
        """Get node-bandwidth subscription for node for user

        Args:
            email (str): Email of the user
            fingerprint (str): Fingerprint of the node

        Raises:
            BadRequest: Bad Request

        Returns:
            Optional[NodeBandwidthSub]: Node Bandwidth Subscription
        """
        user_subs = cls.get_subs_for_user(email)
        sub_with_fp = [s for s in user_subs if s.subscription.relay.fingerprint == fingerprint]
        if len(sub_with_fp) == 0:
            return None
        return sub_with_fp.pop()

    @staticmethod
    def get_subs_for_user(email: str) -> List[NodeBandwidthSub]:
        """Get node-bandwidth subscriptions for user

        Args:
            email (str): Email of the user

        Raises:
            UnAuthorized: Unauthorized access

        Returns:
            List[NodeBandwidthSub]: List of Node Bandwidth Subscription
        """
        user = SubscriberDAO.get_user_by_email(email)

        if not user:
            raise UnAuthorized()

        subscriptions = user.subscriptions
        return [
            subscription.node_bandwidth_sub
            for subscription in subscriptions
            if subscription.node_bandwidth_sub
        ]

    @staticmethod
    def create(email: str, sub: NodeBandwidthSubDTO) -> None:
        subscription = SubscriptionDAO.get_subscription_for_user_fp(
            email=email, fingerprint=sub.fingerprint
        )
        node_down = NodeBandwidthSub(
            subscription=subscription, wait_for=sub.wait_for, threshold=sub.threshold
        )  # type: ignore
        try:
            db.session.add(node_down)
            db.session.commit()
        except exc.SQLAlchemyError as e:
            app_logger.error(f"Adding node-bandwidth-sub to db failed - {e}")
            db.session.rollback()

    @classmethod
    def delete(cls, subscription: NodeBandwidthSub) -> None:
        db.session.delete(subscription)
        cls.commit()

    @staticmethod
    def commit() -> None:
        db.session.commit()
