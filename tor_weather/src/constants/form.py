from enum import Enum
from typing import List, Optional

from attr import dataclass

from tor_weather.src.dto.common import Flag


class FORM_FIELD(Enum):
    FINGERPRINT = "fingerprint"
    WAIT_FOR = "wait_for"
    THRESHOLD_BANDWIDTH = "threshold"
    FLAG = "flag"


class FORM_BUTTON(Enum):
    CREATE = "Create"
    MODIFY = "Modify"


@dataclass(kw_only=True)
class InternalFormFieldDTO:
    field_type: str
    label: str
    value: str
    default: Optional[str] = None


@dataclass
class TextFormFieldDTO(InternalFormFieldDTO):
    field_type: str = "text"


@dataclass
class NumberFormFieldDTO(InternalFormFieldDTO):
    min_value: int
    field_type: str = "number"


@dataclass
class SelectOptionDTO:
    label: str
    value: str


@dataclass
class SelectFormFieldDTO(InternalFormFieldDTO):
    options: List[SelectOptionDTO]
    field_type: str = "select"


@dataclass
class FormFieldDTO:
    value: str
    input_type: str
    display_name: str
    disabled: bool
    default_value: Optional[str | int] = None
    options: Optional[List[SelectOptionDTO]] = None


@dataclass
class FormDTO:
    header: str
    endpoint: str
    button: str
    fields: List[FormFieldDTO]


FORM_MAP: dict[FORM_FIELD, InternalFormFieldDTO] = {
    FORM_FIELD.FINGERPRINT: TextFormFieldDTO(
        label="Fingerprint", value=FORM_FIELD.FINGERPRINT.value
    ),
    FORM_FIELD.WAIT_FOR: NumberFormFieldDTO(
        label="Wait For (hrs)", value=FORM_FIELD.WAIT_FOR.value, min_value=1
    ),
    FORM_FIELD.THRESHOLD_BANDWIDTH: NumberFormFieldDTO(
        label="Threshold Bandwidth (KBps)", value=FORM_FIELD.THRESHOLD_BANDWIDTH.value, min_value=1
    ),
    FORM_FIELD.FLAG: SelectFormFieldDTO(
        label="Flag",
        value=FORM_FIELD.FLAG.value,
        options=[
            SelectOptionDTO(label=Flag.Fast.name, value=Flag.Fast.value),
            SelectOptionDTO(label=Flag.Exit.name, value=Flag.Exit.value),
            SelectOptionDTO(label=Flag.Guard.name, value=Flag.Guard.value),
            SelectOptionDTO(label=Flag.Stable.name, value=Flag.Stable.value),
        ],
    ),
}
