from enum import Enum
from typing import List, Optional

from attr import dataclass


class SIDEBAR_CATEGORY(Enum):
    NODE_STATUS = "Node Status"
    EVENTS = "Events"
    SUPPORT = "Support"


class PAGE(Enum):
    NODE_DOWN = "Node Down"
    NODE_BANDWIDTH = "Node Bandwidth"
    NODE_FLAG = "Node Flag"
    NODE_VERSION = "Node Version"

    RELAY_OPERATORS_MEETUP = "Relay Operators Meetup"
    CLAIM_TSHIRT_AWARD = "Claim T-Shirt Award"
    MONTHLY_STATS = "Monthly Stats"
    LEADERBOARD = "Leaderboard"

    GET_HELP = "Get Help"
    SUBMIT_FEEDBACK = "Submit Feedback"
    LOGOUT = "Logout"


@dataclass
class SidebarEntryDTO:
    display_name: str
    url: str
    icon: str
    is_active: Optional[bool] = False


@dataclass
class SidebarIntenalEntryDTO:
    display_name: str
    url: str
    icon: str
    category: SIDEBAR_CATEGORY


@dataclass
class SidebarCategoryDTO:
    display_name: str
    options: List[SidebarEntryDTO]


@dataclass
class SidebarDTO:
    categories: List[SidebarCategoryDTO]


PAGE_MAP = {
    PAGE.NODE_DOWN: SidebarIntenalEntryDTO(
        display_name=PAGE.NODE_DOWN.value,
        category=SIDEBAR_CATEGORY.NODE_STATUS,
        url="/dashboard/node-status/node-down",
        icon="trending_down",
    ),
    PAGE.NODE_BANDWIDTH: SidebarIntenalEntryDTO(
        display_name=PAGE.NODE_BANDWIDTH.value,
        category=SIDEBAR_CATEGORY.NODE_STATUS,
        url="/dashboard/node-status/node-bandwidth",
        icon="speed",
    ),
    PAGE.NODE_FLAG: SidebarIntenalEntryDTO(
        display_name=PAGE.NODE_FLAG.value,
        category=SIDEBAR_CATEGORY.NODE_STATUS,
        url="/dashboard/node-status/node-flag",
        icon="outlined_flag",
    ),
    PAGE.NODE_VERSION: SidebarIntenalEntryDTO(
        display_name=PAGE.NODE_VERSION.value,
        category=SIDEBAR_CATEGORY.NODE_STATUS,
        url="/dashboard/node-status/node-version",
        icon="browser_updated",
    ),
    PAGE.RELAY_OPERATORS_MEETUP: SidebarIntenalEntryDTO(
        display_name=PAGE.RELAY_OPERATORS_MEETUP.value,
        category=SIDEBAR_CATEGORY.EVENTS,
        url="/dashboard/events/relay-operators-meetup",
        icon="groups_3",
    ),
    PAGE.CLAIM_TSHIRT_AWARD: SidebarIntenalEntryDTO(
        display_name=PAGE.CLAIM_TSHIRT_AWARD.value,
        category=SIDEBAR_CATEGORY.EVENTS,
        url="/dashboard/events/earned-tshirt",
        icon="emoji_events",
    ),
    PAGE.MONTHLY_STATS: SidebarIntenalEntryDTO(
        display_name=PAGE.MONTHLY_STATS.value,
        category=SIDEBAR_CATEGORY.EVENTS,
        url="/dashboard/events/monthly-stats",
        icon="query_stats",
    ),
    PAGE.LEADERBOARD: SidebarIntenalEntryDTO(
        display_name=PAGE.LEADERBOARD.value,
        category=SIDEBAR_CATEGORY.EVENTS,
        url="/dashboard/events/leaderboard",
        icon="leaderboard",
    ),
    PAGE.GET_HELP: SidebarIntenalEntryDTO(
        display_name=PAGE.GET_HELP.value,
        category=SIDEBAR_CATEGORY.SUPPORT,
        url="https://forum.torproject.org/c/support/relay-operator/",
        icon="info",
    ),
    PAGE.SUBMIT_FEEDBACK: SidebarIntenalEntryDTO(
        display_name=PAGE.SUBMIT_FEEDBACK.value,
        category=SIDEBAR_CATEGORY.SUPPORT,
        url="https://gitlab.torproject.org/tpo/network-health/tor-weather/-/issues",
        icon="chat_bubble_outline",
    ),
    PAGE.LOGOUT: SidebarIntenalEntryDTO(
        display_name=PAGE.LOGOUT.value,
        category=SIDEBAR_CATEGORY.SUPPORT,
        url="/api/logout",
        icon="logout",
    ),
}
