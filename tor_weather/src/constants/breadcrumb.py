from enum import Enum


class BREADCRUMB_LEVEL(Enum):
    CREATE = "Create"
    MODIFY = "Modify"
