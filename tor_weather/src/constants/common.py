from dataclasses import dataclass
from enum import Enum


class ISubscriptionCategory(Enum):
    NODE_DOWN = "node_down"
    NODE_BANDWIDTH = "node_bandwidth"
    NODE_VERSION = "node_version"
    NODE_FLAG = "node_flag"
    NODE_STATUS = "node_status"


class MessageResponseTypes(Enum):
    SUCCESS = "success"
    WARNING = "warning"
    ERROR = "error"


@dataclass
class INotification:
    message: str
    category: str


class ISubscription(Enum):
    NODE_DOWN = "node_down"
    NODE_BANDWIDTH = "node_bandwidth"
    NODE_FLAG_EXIT = "node_flag_exit"
    NODE_FLAG_FAST = "node_flag_fast"
    NODE_FLAG_GUARD = "node_flag_guard"
    NODE_FLAG_STABLE = "node_flag_stable"


class SnackbarMessage(Enum):
    EMAIL_VERIFICATION_SUCCESS = INotification(
        message="Account was successfully verified. Please proceed to login.",
        category=MessageResponseTypes.SUCCESS.value,
    )
    EMAIL_VERIFICATION_REPEAT = INotification(
        message="User was already verified. You can proceed to Login",
        category=MessageResponseTypes.WARNING.value,
    )
    USER_ALREADY_EXISTS = INotification(
        message="User with this email already exists! Please login instead.",
        category=MessageResponseTypes.WARNING.value,
    )
    VERIFICATION_EMAIL_SENT = INotification(
        message="A verification email has been sent. Click on the link sent to continue.",
        category=MessageResponseTypes.SUCCESS.value,
    )
    VERIFICATION_EMAIL_RESEND = INotification(
        message="""Your email verification is still pending. We have sent a new email,
                             please click on the link to activate your account.""",
        category=MessageResponseTypes.WARNING.value,
    )
    USER_NOT_FOUND = INotification(
        message="There is no user with this email id. Please register for a new account.",
        category=MessageResponseTypes.ERROR.value,
    )
    INCORRECT_CREDENTIALS = INotification(
        message="Incorrect username, password combination",
        category=MessageResponseTypes.ERROR.value,
    )
    SUBSCRIPTION_CREATED = INotification(
        message="Subscription successfully created!",
        category=MessageResponseTypes.SUCCESS.value,
    )
    SUBSCRIPTION_MODIFIED = INotification(
        message="Subscription successfully modified!",
        category=MessageResponseTypes.SUCCESS.value,
    )
    SUBSCRIPTION_DELETED = INotification(
        message="Subscription successfully deleted!",
        category=MessageResponseTypes.SUCCESS.value,
    )
    SUBSCRIPTION_DISABLED = INotification(
        message="Subscription successfully disabled!",
        category=MessageResponseTypes.SUCCESS.value,
    )
    SUBSCRIPTION_ENABLED = INotification(
        message="Subscription successfully enabled!",
        category=MessageResponseTypes.SUCCESS.value,
    )
    SUBSCRIPTION_DOES_NOT_EXIST = INotification(
        message="Subscription does not exist. Create one instead!",
        category=MessageResponseTypes.ERROR.value,
    )
    RELAY_NOT_FOUND = INotification(
        message="""Provided fingerprint doesn't match a relay. If the relay was recently created, please wait for
        some time for it to be captured by the service.""",
        category=MessageResponseTypes.WARNING.value,
    )
    SUBSCRIPTION_EXISTS = INotification(
        message="A similar subscription exists. Edit it instead!",
        category=MessageResponseTypes.ERROR.value,
    )
    WRONG_FINGERPRINT = INotification(
        message="Invalid Fingerprint ID was provided.",
        category=MessageResponseTypes.ERROR.value,
    )


TEMPLATE_NS_DISC: str = "External Routes containing templates to be called by the end-users"
EXTERNAL_NS_DISC: str = "External Routes for form actions to be called by the end-users"
