from enum import Enum
from typing import Optional

from attr import dataclass


@dataclass
class InfoCardDTO:
    header: str
    content: str
    link: Optional[str] = None


class INFO_CARD_KEY(Enum):
    FINGERPRINT = "fingerprint"
    WAIT_FOR = "wait_for"
    THRESHOLD = "threshold"
    FLAG = "flag"


INFO_CARD_DATA = {
    INFO_CARD_KEY.FINGERPRINT.value: InfoCardDTO(
        header="Fingerprint",
        content="""Every node is assigned a unique 40-digit uppercase ID. You can locate your relay's fingerprint by
        clicking.""",
        link="https://metrics.torproject.org/rs.html#search",
    ),
    INFO_CARD_KEY.WAIT_FOR.value: InfoCardDTO(
        header="Waiting Time",
        content="""The service will wait for a designated period after initially detecting an anomaly with the relay.
        If the issue remains unresolved within this timeframe, a notification email will be sent.""",
    ),
    INFO_CARD_KEY.THRESHOLD.value: InfoCardDTO(
        header="Threshold Bandwidth",
        content="""The service will identify an anomaly if the average bandwidth of the relay drops below the specified
        threshold in the subscription.""",
    ),
    INFO_CARD_KEY.FLAG.value: InfoCardDTO(
        header="Flag",
        content="""The service will flag an anomaly if the selected node is missing the specified flag.""",
    ),
}
