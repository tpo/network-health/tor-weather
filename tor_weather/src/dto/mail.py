from dataclasses import dataclass
from typing import List


@dataclass
class MailDTO:
    from_email: str
    to: List[str]
    subject: str
    html_body: str
    text_body: str
