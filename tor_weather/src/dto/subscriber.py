from dataclasses import dataclass
from typing import Optional

from tor_weather.src.constants.common import INotification


@dataclass
class LoginDTO:
    redirect_url: str
    notification: Optional[INotification] = None


@dataclass
class LogoutDTO:
    redirect_url: str
    notification: Optional[INotification] = None


@dataclass
class RegisterDTO:
    redirect_url: str
    notification: Optional[INotification] = None


@dataclass
class VerificationDTO:
    redirect_url: str
    notification: Optional[INotification] = None


@dataclass
class SubscriberDTO:
    id: int
    email: str
    is_confirmed: bool
