from typing import List

from attr import dataclass


@dataclass
class ITableHeader:
    label: str
    value: str


@dataclass
class ITableIconCellObject:
    name: str
    type: str
    url: str


@dataclass
class ITableTextCell:
    text: str


@dataclass
class ITableIconCell:
    icon: ITableIconCellObject


@dataclass
class ITableRow:
    pass


@dataclass
class ITable:
    headers: List[ITableHeader]
    content: List[ITableRow]
