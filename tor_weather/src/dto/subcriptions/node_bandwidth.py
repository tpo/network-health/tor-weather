from dataclasses import dataclass

from tor_weather.src.dto.presentation.table import ITableIconCell, ITableRow, ITableTextCell


@dataclass
class NodeBandwidthSubDTO:
    fingerprint: str
    wait_for: int
    threshold: int


@dataclass
class UINodeBandwidthListItem:
    fingerprint: str
    wait_for: int
    is_active: bool
    threshold: int


@dataclass
class INodeBandwidthTableRow(ITableRow):
    fingerprint: ITableTextCell
    wait_for: ITableTextCell
    threshold: ITableTextCell
    is_active: ITableIconCell
    edit: ITableIconCell
    delete: ITableIconCell
