from dataclasses import dataclass

from tor_weather.src.dto.presentation.table import ITableIconCell, ITableRow, ITableTextCell


@dataclass
class NodeFlagSubDTO:
    fingerprint: str
    wait_for: int
    flag: str


@dataclass
class UINodeFlagListItem:
    fingerprint: str
    wait_for: int
    is_active: bool


@dataclass
class INodeFlagTableRow(ITableRow):
    fingerprint: ITableTextCell
    wait_for: ITableTextCell
    flag: ITableTextCell
    is_active: ITableIconCell
    edit: ITableIconCell
    delete: ITableIconCell
