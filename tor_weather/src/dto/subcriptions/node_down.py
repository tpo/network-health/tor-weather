from dataclasses import dataclass

from tor_weather.src.dto.presentation.table import ITableIconCell, ITableRow, ITableTextCell


@dataclass
class NodeDownSubDTO:
    fingerprint: str
    wait_for: int


@dataclass
class UINodeDownListItem:
    fingerprint: str
    wait_for: int
    is_active: bool


@dataclass
class INodeDownTableRow(ITableRow):
    fingerprint: ITableTextCell
    wait_for: ITableTextCell
    is_active: ITableIconCell
    edit: ITableIconCell
    delete: ITableIconCell
