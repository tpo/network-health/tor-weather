from dataclasses import dataclass


@dataclass
class InsertSubscriptionDTO:
    fingerprint: str
    email: str
