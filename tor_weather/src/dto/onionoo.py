from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional


@dataclass(kw_only=True)
class RawOnionooBridgeInterface:
    running: bool
    nickname: str
    last_seen: str
    first_seen: str
    hashed_fingerprint: str
    or_addresses: List[str]
    contact: Optional[str] = None
    version: Optional[str] = None
    platform: Optional[str] = None
    flags: Optional[List[str]] = None
    last_restarted: Optional[str] = None
    version_status: Optional[str] = None
    blocklist: Optional[List[str]] = None
    transports: Optional[List[str]] = None
    recommended_version: Optional[bool] = None
    advertised_bandwidth: Optional[int] = None
    bridgedb_distributor: Optional[str] = None
    overload_general_timestamp: Optional[int] = None


@dataclass(kw_only=True)
class OnionooBridgeInterface(RawOnionooBridgeInterface):
    fingerprint: str
    first_seen: datetime  # type: ignore
    last_seen: datetime  # type: ignore


@dataclass(kw_only=True)
class RawOnionooRelayInterface:
    nickname: str
    fingerprint: str
    or_addresses: List[str]
    last_seen: str
    last_changed_address_or_port: str
    first_seen: str
    running: bool
    consensus_weight: int
    exit_addresses: Optional[List[str]] = None
    dir_address: Optional[str] = None
    hibernating: Optional[bool] = None
    flags: Optional[List[str]] = None
    country: Optional[str] = None
    country_name: Optional[str] = None
    region_name: Optional[str] = None
    city_name: Optional[str] = None
    latitude: Optional[int] = None
    longitude: Optional[int] = None
    as_number: Optional[str] = None
    as_name: Optional[str] = None
    verified_host_names: Optional[List[str]] = None
    unverified_host_names: Optional[List[str]] = None
    last_restarted: Optional[str] = None
    bandwidth_rate: Optional[int] = None
    bandwidth_burst: Optional[int] = None
    observed_bandwidth: Optional[int] = None
    advertised_bandwidth: Optional[int] = None
    overload_general_timestamp: Optional[int] = None
    exit_policy: Optional[List[str]] = None
    exit_policy_summary: Optional[dict] = None
    exit_policy_v6_summary: Optional[dict] = None
    contact: Optional[str] = None
    platform: Optional[str] = None
    version: Optional[str] = None
    recommended_version: Optional[bool] = None
    version_status: Optional[str] = None
    effective_family: Optional[List[str]] = None
    alleged_family: Optional[List[str]] = None
    indirect_family: Optional[List[str]] = None
    consensus_weight_fraction: Optional[int] = None
    guard_probability: Optional[int] = None
    middle_probability: Optional[int] = None
    exit_probability: Optional[int] = None
    measured: Optional[bool] = None
    unreachable_or_addresses: Optional[List[str]] = None


@dataclass(kw_only=True)
class OnionooRelayInterface(RawOnionooRelayInterface):
    first_seen: datetime  # type: ignore
    last_seen: datetime  # type: ignore


@dataclass(kw_only=True)
class OnionooInterface:
    bridges: List[RawOnionooBridgeInterface]
    bridges_published: str
    build_revision: str
    relays: List[RawOnionooRelayInterface]
    relays_published: str
    version: str


OnionooNodeInterface = OnionooRelayInterface | OnionooBridgeInterface
