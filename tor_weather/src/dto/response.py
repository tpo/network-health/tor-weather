from dataclasses import dataclass
from typing import Any, Optional

from tor_weather.src.constants.common import INotification


@dataclass
class RedirectResponseDTO:
    redirect_url: str
    notification: Optional[INotification] = None


@dataclass
class TemplateResponseDTO:
    template_url: str
    data: Optional[dict[str, Any]] = None
