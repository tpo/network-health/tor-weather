from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class InsertRelayDTO:
    fingerprint: str
    first_seen: datetime
    nickname: Optional[str] = None
    is_bridge: bool = False
