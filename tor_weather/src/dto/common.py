from enum import Enum


class Flag(Enum):
    Fast = "fast"
    Exit = "exit"
    Guard = "guard"
    Stable = "stable"
