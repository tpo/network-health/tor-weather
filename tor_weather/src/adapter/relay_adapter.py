from tor_weather.src.dto.onionoo import OnionooNodeInterface
from tor_weather.src.dto.relay import InsertRelayDTO


class RelayAdapter:
    @staticmethod
    def onionoo_to_dto(relay: OnionooNodeInterface, is_bridge: bool) -> InsertRelayDTO:
        return InsertRelayDTO(
            fingerprint=relay.fingerprint,
            nickname=relay.nickname,
            first_seen=relay.first_seen,
            is_bridge=is_bridge,
        )
