from abc import abstractmethod

from flask_login import current_user

from tor_weather.extensions import app_logger
from tor_weather.src.constants.common import INotification, MessageResponseTypes
from tor_weather.src.dto.response import RedirectResponseDTO


class APIError(Exception):
    """API Error Exception

    Args:
        Exception (_type_): Python's Default Exception
    """

    def __init__(self, message: str, status_code=500):
        """Initialises the API Error Exception

        Args:
            message (str): The message to be sent with the error
            status_code (int, optional): Status code for the error. Defaults to 500.
        """
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code

    @abstractmethod
    def get_response(self) -> RedirectResponseDTO:
        pass


class BadRequest(APIError):
    """API Error Exception for Bad Request

    Args:
        APIError (_type_): Base API Error Class
    """

    def __init__(self, message: str) -> None:
        """Initialises the Bad Request Exception

        Args:
            message (str): The message to be sent with the error
        """
        status_code = 400
        APIError.__init__(self, message, status_code)

    def get_response(self) -> RedirectResponseDTO:
        app_logger.error(self.status_code, self.message)
        redirect_url = "dashboard" if current_user.is_authenticated else "login"  # type: ignore
        return RedirectResponseDTO(
            redirect_url=redirect_url,
            notification=INotification(
                message=self.message, category=MessageResponseTypes.ERROR.value
            ),
        )


class UnAuthorized(APIError):
    """API Error Exception for Unauthorized Request

    Args:
        APIError (_type_): Base API Error Class
    """

    def __init__(self, message: str = "Unauthorized") -> None:
        """Initialises the Unauthorized Exception

        Args:
            message (str): The message to be sent with the error
        """
        status_code = 401
        APIError.__init__(self, message, status_code)

    def get_response(self) -> RedirectResponseDTO:
        app_logger.error(self.status_code, self.message)
        redirect_url = "login"  # type: ignore
        return RedirectResponseDTO(
            redirect_url=redirect_url,
            notification=INotification(
                message=self.message or "You are not authorized for that",
                category=MessageResponseTypes.ERROR.value,
            ),
        )


class InternalError(APIError):
    """API Error Exception for Internal Error

    Args:
        APIError (_type_): Base API Error Class
    """

    message = "Something went wrong."

    def __init__(self, message: str = message) -> None:
        """Initialises the Internal Error Exception

        Args:
            message (str): The message to be sent with the error
        """
        status_code = 500
        APIError.__init__(self, message, status_code)

    def get_response(self) -> RedirectResponseDTO:
        app_logger.exception(self.status_code, self.message)
        redirect_url = "dashboard" if current_user.is_authenticated else "login"  # type: ignore
        return RedirectResponseDTO(
            redirect_url=redirect_url,
            notification=INotification(
                message="Something went wrong!",
                category=MessageResponseTypes.ERROR.value,
            ),
        )
