from typing import Optional

from flask_restx import Api, Namespace

from tor_weather.src.constants.common import EXTERNAL_NS_DISC, TEMPLATE_NS_DISC

TEMPLATE_NS: Optional[Namespace] = None
API_NS: Optional[Namespace] = None


def template_ns() -> Namespace:
    """Get the Template Namespace

    Raises:
        Exception: If the Template Namespace is accessed before initialisation

    Returns:
        Namespace: Template Namespace
    """
    global TEMPLATE_NS
    if not TEMPLATE_NS:
        raise Exception("Template Namespace is not yet defined")
    else:
        return TEMPLATE_NS


def api_ns() -> Namespace:
    """Get the API Namespace

    Raises:
        Exception: If the API Namespace is accessed before initialisation

    Returns:
        Namespace: API Namespace
    """
    global API_NS
    if not API_NS:
        raise Exception("API Namespace is not yet defined")
    else:
        return API_NS


def configure_namespaces(api: Api) -> None:
    """Configures the namespaces

    Args:
        api (Api): Flask Restx API Instance
    """
    global TEMPLATE_NS, API_NS
    TEMPLATE_NS = Namespace("template_routes", description=TEMPLATE_NS_DISC)
    API_NS = Namespace("api_routes", description=EXTERNAL_NS_DISC)
    api.add_namespace(TEMPLATE_NS, path="/")
    api.add_namespace(API_NS, path="/api/")


def get_routes():
    from tor_weather.resources.api.auth.login import LoginApi
    from tor_weather.resources.api.auth.logout import LogoutApi
    from tor_weather.resources.api.auth.register import RegisterApi
    from tor_weather.resources.api.auth.verify import VerifyApi
    from tor_weather.resources.api.subscription.create.node_bandwidth import (
        NodeBandwidthSubscriptionCreateApi,
    )
    from tor_weather.resources.api.subscription.create.node_down import (
        NodeDownSubscriptionCreateApi,
    )
    from tor_weather.resources.api.subscription.create.node_flag import (
        NodeFlagSubscriptionCreateApi,
    )
    from tor_weather.resources.api.subscription.delete.node_bandwidth import (
        NodeBandwidthSubscriptionDeleteApi,
    )
    from tor_weather.resources.api.subscription.delete.node_down import (
        NodeDownSubscriptionDeleteApi,
    )
    from tor_weather.resources.api.subscription.delete.node_flag import (
        NodeFlagSubscriptionDeleteApi,
    )
    from tor_weather.resources.api.subscription.disable.node_bandwidth import (
        NodeBandwidthSubscriptionDisableApi,
    )
    from tor_weather.resources.api.subscription.disable.node_down import (
        NodeDownSubscriptionDisableApi,
    )
    from tor_weather.resources.api.subscription.disable.node_flag import (
        NodeFlagSubscriptionDisableApi,
    )
    from tor_weather.resources.api.subscription.enable.node_bandwidth import (
        NodeBandwidthSubscriptionEnableApi,
    )
    from tor_weather.resources.api.subscription.enable.node_down import (
        NodeDownSubscriptionEnableApi,
    )
    from tor_weather.resources.api.subscription.enable.node_flag import (
        NodeFlagSubscriptionEnableApi,
    )
    from tor_weather.resources.api.subscription.modify.node_bandwidth import (
        NodeBandwidthSubscriptionModifyApi,
    )
    from tor_weather.resources.api.subscription.modify.node_down import (
        NodeDownSubscriptionModifyApi,
    )
    from tor_weather.resources.api.subscription.modify.node_flag import (
        NodeFlagSubscriptionModifyApi,
    )
    from tor_weather.resources.template.dashboard import DashboardHomeTemplate
    from tor_weather.resources.template.login import LoginTemplate
    from tor_weather.resources.template.register import RegisterTemplate
    from tor_weather.resources.template.subscription.create.node_bandwidth import (
        NodeBandwidthCreateTemplate,
    )
    from tor_weather.resources.template.subscription.create.node_down import NodeDownCreateTemplate
    from tor_weather.resources.template.subscription.create.node_flag import NodeFlagCreateTemplate
    from tor_weather.resources.template.subscription.list.node_bandwidth import (
        NodeBandwidthListTemplate,
    )
    from tor_weather.resources.template.subscription.list.node_down import NodeDownListTemplate
    from tor_weather.resources.template.subscription.list.node_flag import NodeFlagListTemplate
    from tor_weather.resources.template.subscription.modify.node_bandwidth import (
        NodeBandwidthModifyTemplate,
    )
    from tor_weather.resources.template.subscription.modify.node_down import NodeDownModifyTemplate
    from tor_weather.resources.template.subscription.modify.node_flag import NodeFlagModifyTemplate
    from tor_weather.resources.template.unavailable import ComingSoonTemplate

    routes = {
        "template_routes": [
            {
                "resource": RegisterTemplate,
                "endpoints": ["register"],
                "endpoint_name": "register",
            },
            {
                "resource": LoginTemplate,
                "endpoints": ["login"],
                "endpoint_name": "login",
            },
            {
                "resource": DashboardHomeTemplate,
                "endpoints": ["dashboard"],
                "endpoint_name": "dashboard_home",
            },
            {
                "resource": NodeDownListTemplate,
                "endpoints": ["dashboard/node-status/node-down"],
                "endpoint_name": "node_down_list",
            },
            {
                "resource": NodeDownCreateTemplate,
                "endpoints": ["dashboard/node-status/node-down/create"],
                "endpoint_name": "node_down_create",
            },
            {
                "resource": NodeDownModifyTemplate,
                "endpoints": ["dashboard/node-status/node-down/<fingerprint>/modify"],
                "endpoint_name": "node_down_modify",
            },
            {
                "resource": NodeBandwidthListTemplate,
                "endpoints": ["dashboard/node-status/node-bandwidth"],
                "endpoint_name": "node_bandwidth_list",
            },
            {
                "resource": NodeBandwidthCreateTemplate,
                "endpoints": ["dashboard/node-status/node-bandwidth/create"],
                "endpoint_name": "node_bandwidth_create",
            },
            {
                "resource": NodeBandwidthModifyTemplate,
                "endpoints": ["dashboard/node-status/node-bandwidth/<fingerprint>/modify"],
                "endpoint_name": "node_bandwidth_modify",
            },
            {
                "resource": NodeFlagListTemplate,
                "endpoints": ["dashboard/node-status/node-flag"],
                "endpoint_name": "node_flag_list",
            },
            {
                "resource": NodeFlagCreateTemplate,
                "endpoints": ["dashboard/node-status/node-flag/create"],
                "endpoint_name": "node_flag_create",
            },
            {
                "resource": NodeFlagModifyTemplate,
                "endpoints": ["dashboard/node-status/node-flag/<flag>/<fingerprint>/modify"],
                "endpoint_name": "node_flag_modify",
            },
            {
                "resource": ComingSoonTemplate,
                "endpoints": [
                    "dashboard/node-status/node-version",
                    "dashboard/events/relay-operators-meetup",
                    "dashboard/events/earned-tshirt",
                    "dashboard/events/monthly-stats",
                    "dashboard/events/leaderboard",
                ],
                "endpoint_name": "coming_soon",
            },
        ],
        "api_routes": [
            {
                "resource": RegisterApi,
                "endpoints": ["register"],
            },
            {
                "resource": LoginApi,
                "endpoints": ["login"],
            },
            {
                "resource": LogoutApi,
                "endpoints": ["logout"],
            },
            {
                "resource": VerifyApi,
                "endpoints": ["verify"],
            },
            # Node Down Subscription Endpoints
            {
                "resource": NodeDownSubscriptionCreateApi,
                "endpoints": ["dashboard/node-status/node-down/create"],
                "endpoint_name": "node_down_create_api",
            },
            {
                "resource": NodeDownSubscriptionModifyApi,
                "endpoints": ["dashboard/node-status/node-down/modify"],
                "endpoint_name": "node_down_modify_api",
            },
            {
                "resource": NodeDownSubscriptionEnableApi,
                "endpoints": ["dashboard/node-status/node-down/<fingerprint>/enable"],
                "endpoint_name": "node_down_enable_api",
            },
            {
                "resource": NodeDownSubscriptionDisableApi,
                "endpoints": ["dashboard/node-status/node-down/<fingerprint>/disable"],
                "endpoint_name": "node_down_disable_api",
            },
            {
                "resource": NodeDownSubscriptionDeleteApi,
                "endpoints": ["dashboard/node-status/node-down/<fingerprint>/delete"],
                "endpoint_name": "node_down_delete_api",
            },
            # Node Bandwidth Subscription Endpoints
            {
                "resource": NodeBandwidthSubscriptionCreateApi,
                "endpoints": ["dashboard/node-status/node-bandwidth/create"],
                "endpoint_name": "node_bandwidth_create_api",
            },
            {
                "resource": NodeBandwidthSubscriptionModifyApi,
                "endpoints": ["dashboard/node-status/node-bandwidth/modify"],
                "endpoint_name": "node_bandwidth_modify_api",
            },
            {
                "resource": NodeBandwidthSubscriptionEnableApi,
                "endpoints": ["dashboard/node-status/node-bandwidth/<fingerprint>/enable"],
                "endpoint_name": "node_bandwidth_enable_api",
            },
            {
                "resource": NodeBandwidthSubscriptionDisableApi,
                "endpoints": ["dashboard/node-status/node-bandwidth/<fingerprint>/disable"],
                "endpoint_name": "node_bandwidth_disable_api",
            },
            {
                "resource": NodeBandwidthSubscriptionDeleteApi,
                "endpoints": ["dashboard/node-status/node-bandwidth/<fingerprint>/delete"],
                "endpoint_name": "node_bandwidth_delete_api",
            },
            # Node Flag Subscription Endpoints
            {
                "resource": NodeFlagSubscriptionCreateApi,
                "endpoints": ["dashboard/node-status/node-flag/create"],
                "endpoint_name": "node_flag_create_api",
            },
            {
                "resource": NodeFlagSubscriptionModifyApi,
                "endpoints": ["dashboard/node-status/node-flag/modify"],
                "endpoint_name": "node_flag_modify_api",
            },
            {
                "resource": NodeFlagSubscriptionEnableApi,
                "endpoints": ["dashboard/node-status/node-flag/<flag>/<fingerprint>/enable"],
                "endpoint_name": "node_flag_enable_api",
            },
            {
                "resource": NodeFlagSubscriptionDisableApi,
                "endpoints": ["dashboard/node-status/node-flag/<flag>/<fingerprint>/disable"],
                "endpoint_name": "node_flag_disable_api",
            },
            {
                "resource": NodeFlagSubscriptionDeleteApi,
                "endpoints": ["dashboard/node-status/node-flag/<flag>/<fingerprint>/delete"],
                "endpoint_name": "node_flag_delete_api",
            },
        ],
    }
    return routes


def add_routes(route_type: Namespace) -> None:
    """
    Register routes for a particular route type
    """
    resources = get_routes()[route_type.name]
    for resource in resources:
        res_obj = resource.get("resource")
        res_endpoints = resource.get("endpoints")
        endpoint = resource.get("endpoint_name")
        if endpoint:
            route_type.add_resource(res_obj, *res_endpoints, endpoint=endpoint)
        else:
            route_type.add_resource(res_obj, *res_endpoints)


def register_routes() -> None:
    """Registers routes under the available namespaces"""
    add_routes(template_ns())
    add_routes(api_ns())
