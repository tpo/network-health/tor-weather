from celery import Celery
from flask_login import LoginManager
from flask_mailman import Mail
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy

from tor_weather.utilities.logging import create_logger

"""Database extension"""
db: SQLAlchemy = SQLAlchemy()

"""Mail extension"""
mail = Mail()

"""Celery extension"""
celery = Celery("tor-weather")

"""Login manager extension"""
login_manager = LoginManager()

"""Flask restx extension"""
api = Api(doc="/documentation")

"""App logger extension"""
app_logger = create_logger("app")

"""Task logger extension"""
task_logger = create_logger("task")
