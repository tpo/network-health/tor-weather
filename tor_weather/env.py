from os import environ

from flask import Flask


def load_env_to_app_config(app: Flask) -> None:
    # This is the port that the application runs on
    app.config["FLASK_RUN_PORT"] = environ.get("FLASK_RUN_PORT", 3000)
    # This is the configuration for using 'flask run'
    app.config["FLASK_APP"] = environ.get("FLASK_APP")
    # This is the secret key for the flask application
    app.config["SECRET_KEY"] = environ.get("SECRET_KEY")
    # This is the host-name for the SMTP Connection
    app.config["MAIL_SERVER"] = environ.get("SMTP_HOST")
    # This is the port number for the SMTP Connection
    app.config["MAIL_PORT"] = environ.get("SMTP_PORT")
    # This is the username for the SMTP Connection
    app.config["MAIL_USERNAME"] = environ.get("SMTP_USERNAME")
    # This is the password for the SMTP Connection
    app.config["MAIL_PASSWORD"] = environ.get("SMTP_PASSWORD")
    # This is the status for using SSL with SMTP
    app.config["MAIL_USE_SSL"] = environ.get("SMTP_SSL", False)
    # This is the URI for the database connection
    app.config["SQLALCHEMY_DATABASE_URI"] = environ.get("SQLALCHEMY_DATABASE_URI")
    # This is the URI for the message broker
    app.config["BROKER_URL"] = environ.get("BROKER_URL")
    # This is the url for fetching the node statuses
    app.config["API_URL"] = environ.get("API_URL")
    # This is the url where the application is hosted.
    app.config["BASE_URL"] = environ.get("BASE_URL")
    # This is the encryption key used for encrypting the information in the verification link.
    app.config["EMAIL_ENCRYPT_PASS"] = environ.get("EMAIL_ENCRYPT_PASS")
    # This is the time interval (in minutes) after which the latest status of nodes is fetched from onionoo
    app.config["ONIONOO_JOB_INTERVAL"] = environ.get("ONIONOO_JOB_INTERVAL")
    # This is required by SqlAlchemy, and should ideally be false
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = environ.get(
        "SQLALCHEMY_TRACK_MODIFICATIONS", False
    )
