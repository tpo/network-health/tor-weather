import base64


def encode_to_base64(payload: str) -> bytes:
    """Encodes a string to url-safe base64

    Args:
        payload (str): String to be encoded

    Returns:
        str: Encoded string
    """
    return base64.urlsafe_b64encode(str.encode(payload[:32]))
