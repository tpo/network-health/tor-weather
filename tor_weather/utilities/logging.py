from logging import WARNING, Formatter, Logger, getLogger
from logging.handlers import TimedRotatingFileHandler


def get_formatter() -> Formatter:
    """Get formatter for logging

    Returns:
        Formatter: logging.Formatter instance
    """
    return Formatter("%(asctime)s : %(levelname)s : %(message)s", "%m/%d/%Y %I:%M:%S %p")


def get_rotating_file_handler(filename: str) -> TimedRotatingFileHandler:
    """Get handler for logging to rotating files

    Args:
        filename (str): Name of the log file

    Returns:
        TimedRotatingFileHandler: TimedRotatingFileHandler instance
    """
    dirname = f"logs/{filename}"
    handler = TimedRotatingFileHandler(dirname, when="midnight", backupCount=30)
    handler.setFormatter(get_formatter())
    return handler


def create_logger(name: str) -> Logger:
    """Create logger instance

    Args:
        name (str): Name of the logger & filename

    Returns:
        Logger: Logger instance
    """
    logger = getLogger(name)
    logger.addHandler(get_rotating_file_handler(name))
    logger.setLevel(WARNING)
    return logger
