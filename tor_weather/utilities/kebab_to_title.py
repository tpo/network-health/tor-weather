def kebab_to_title(input: str) -> str:
    """Convert a string from kebab case to title case"""
    return input.replace("-", " ").title()
