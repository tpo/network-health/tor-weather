from datetime import datetime, timedelta


def time_diff(start_time: datetime, end_time: datetime) -> float:
    diff: timedelta = end_time - start_time
    return diff.total_seconds() / 3600
