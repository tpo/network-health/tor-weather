import bcrypt


def hash_password(password: str) -> str:
    salt: bytes = bcrypt.gensalt()
    hashed_password: bytes = bcrypt.hashpw(password.encode("utf-8"), salt)
    return hashed_password.decode("utf-8")


def check_password(user_password: str, database_password: str) -> bool:
    return bcrypt.checkpw(user_password.encode("utf-8"), database_password.encode("utf-8"))
