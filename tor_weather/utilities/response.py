from abc import ABC, abstractmethod
from dataclasses import asdict
from typing import Any

from flask import Response as FlaskResponse
from flask import flash, make_response, redirect, render_template

from tor_weather.src.dto.response import RedirectResponseDTO, TemplateResponseDTO


class Response(ABC):
    def __init__(self, payload, message=None, status=200) -> None:
        self.payload = payload
        self.message = message
        self.status = status

    @abstractmethod
    def to_response(self) -> FlaskResponse:
        pass


class RedirectResponse(Response):
    def __init__(self, payload: RedirectResponseDTO, message=None, status=302) -> None:
        super().__init__(payload, message, status)

    def to_response(self) -> FlaskResponse:
        if self.payload.notification:
            flash(**asdict(self.payload.notification))
        return make_response(redirect(self.payload.redirect_url), self.status)


class TemplateResponse(Response):
    def __init__(self, payload: TemplateResponseDTO, message=None, status=200) -> None:
        super().__init__(payload, message, status)

    def to_response(self) -> FlaskResponse:
        if self.payload.data:
            return make_response(
                render_template(self.payload.template_url, **self.payload.data), self.status
            )
        else:
            return make_response(render_template(self.payload.template_url), self.status)


class APIResponse(Response):
    def __init__(self, payload: Any, message=None, status=200) -> None:
        super().__init__(payload, message, status)

    def to_response(self):
        pass
