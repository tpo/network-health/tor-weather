from typing import Any

import requests
from requests import Response


def http_request(endpoint: str) -> Any:
    response: Response = requests.get(endpoint, timeout=30)
    return response.json()
