from datetime import datetime

from tor_weather.extensions import db


class Relay(db.Model):  # type: ignore
    id: int = db.Column(db.Integer, primary_key=True, nullable=False)
    fingerprint: str = db.Column(db.String(40), nullable=False, unique=True)
    nickname: str = db.Column(db.String(), nullable=True)
    first_seen: datetime = db.Column(db.DateTime, nullable=False)
    is_bridge: bool = db.Column(db.Boolean, nullable=False, default=False)
    # SqlAlchemy Relations
    subscriptions = db.relationship(
        "Subscription", foreign_keys="Subscription.relay_id", backref="relay", lazy=True
    )
    hosts = db.relationship(
        "Subscription", foreign_keys="Subscription.host_id", backref="host", lazy=True
    )
