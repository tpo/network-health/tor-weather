from datetime import datetime

from tor_weather.extensions import db


class NodeFlagExitSub(db.Model):  # type: ignore
    # The primary id for the node down subscription
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    # If the subscription is active (controlled by the subscriber from the dashboard)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    # Creation date for the subscription
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    # When was the subscription last updated by the user
    updated_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    # How long (hrs) to wait for before dispatching off an email
    wait_for = db.Column(db.Integer, nullable=False)
    # When was the issue (node being down) first recorded by Tor-Weather
    issue_first_seen = db.Column(db.DateTime, nullable=True)
    # If we have already sent them an email for the anomaly
    emailed = db.Column(db.Boolean, nullable=False, default=False)
    # Foreign key for mapping it with the subscriptions table
    subscription_id = db.Column(db.Integer, db.ForeignKey("subscription.id"), nullable=False)

    subscription = db.relationship("Subscription", back_populates="node_flag_exit_sub")
