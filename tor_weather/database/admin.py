from datetime import datetime

from tor_weather.extensions import db


class Admin(db.Model):  # type: ignore
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    email = db.Column(db.String(64), nullable=False)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_login = db.Column(db.DateTime, nullable=True)
