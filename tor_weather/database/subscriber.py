from datetime import datetime

from flask_login import UserMixin

from tor_weather.extensions import db


class Subscriber(UserMixin, db.Model):  # type: ignore
    id: int = db.Column(db.Integer, primary_key=True, nullable=False)
    email: str = db.Column(db.String(64), nullable=False)
    password: str = db.Column(db.String(64), nullable=False)
    is_confirmed: bool = db.Column(db.Boolean, nullable=False, default=False)
    created_on: datetime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_on: datetime = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    subscriptions = db.relationship("Subscription", backref="subscriber", lazy=True, uselist=True)
