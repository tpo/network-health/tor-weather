from datetime import datetime
from typing import Any, List, Optional, cast

from flask import current_app

from tor_weather.src.dto.onionoo import (
    OnionooBridgeInterface,
    OnionooInterface,
    OnionooRelayInterface,
    RawOnionooBridgeInterface,
    RawOnionooRelayInterface,
)
from tor_weather.utilities import http_request


class OnionooApi:
    """Class representing the Onionoo Response"""

    def __init__(self) -> None:
        pass

    @classmethod
    def get_response(cls) -> OnionooInterface:
        """Get response from the onionoo metrics api

        Returns:
            OnionooInterface: Onionoo response object
        """
        endpoint: str = f"{current_app.config['API_URL']}/details"
        response = http_request(endpoint)
        parsed_response = cls._replace_reserved_keywords(response)
        return OnionooInterface(**parsed_response)  # type: ignore

    @staticmethod
    def _replace_reserved_keywords(response: Any) -> OnionooInterface:
        """Replaces python-based reserved keywords from onionoo response

        Args:
            response (Any): Response from Onionoo api

        Returns:
            OnionooInterface: Onionoo Response object
        """
        relays = response["relays"]
        for relay in relays:
            relay["as_number"] = relay.pop("as", None)
        return response

    @staticmethod
    def _parse_date(date: str) -> datetime:
        """Parses the date into datetime object.

        Args:
            date (str): Datetime in string format

        Returns:
            datetime: Datetime in datetime format
        """
        return datetime.strptime(date, "%Y-%m-%d %H:%M:%S")

    @staticmethod
    def _change_bandwidth_units(bandwidth: Optional[int]) -> Optional[int]:
        """Change bandwidth units from Bps to kBps

        Args:
            bandwidth (Optional[int]): Bandwidth in Bps

        Returns:
            Optional[int]: Bandwidth in kBps
        """
        bandwidth = bandwidth // 1000 if bandwidth else None
        return bandwidth

    @classmethod
    def _parse_relay(cls, relay: RawOnionooRelayInterface) -> OnionooRelayInterface:
        """Parse onionoo relay object

        Args:
            relay (RawOnionooRelayInterface): Onionoo relay object

        Returns:
            OnionooRelayInterface: Parsed relay object
        """
        # Parse dates from strings
        relay.first_seen = cls._parse_date(relay.first_seen)  # type: ignore
        relay.last_seen = cls._parse_date(relay.last_seen)  # type: ignore
        # Parse bandwidth units
        relay.observed_bandwidth = cls._change_bandwidth_units(relay.observed_bandwidth)
        relay.bandwidth_burst = cls._change_bandwidth_units(relay.bandwidth_burst)
        relay.bandwidth_rate = cls._change_bandwidth_units(relay.bandwidth_rate)
        relay.advertised_bandwidth = cls._change_bandwidth_units(relay.advertised_bandwidth)
        # Convert flags to lower-case
        relay.flags = [flag.lower() for flag in relay.flags] if relay.flags else None
        return cast(OnionooRelayInterface, relay)

    @classmethod
    def _parse_bridge(cls, bridge: RawOnionooBridgeInterface) -> OnionooBridgeInterface:
        """Parse onionoo bridge object

        Args:
            bridge (RawOnionooBridgeInterface): Onionoo bridge object

        Returns:
            OnionooBridgeInterface: Parsed bridge object
        """
        # Parse dates from strings
        bridge.first_seen = cls._parse_date(bridge.first_seen)  # type: ignore
        bridge.last_seen = cls._parse_date(bridge.last_seen)  # type: ignore
        bridge.fingerprint = bridge.hashed_fingerprint  # type: ignore
        # Parse bandwidth units
        bridge.advertised_bandwidth = cls._change_bandwidth_units(bridge.advertised_bandwidth)
        # Convert flags to lower-case
        bridge.flags = [flag.lower() for flag in bridge.flags] if bridge.flags else None
        return cast(OnionooBridgeInterface, bridge)

    @classmethod
    def get_relays(cls, response: OnionooInterface) -> List[OnionooRelayInterface]:
        """Get list of all the relays from the onionoo api call

        Args:
            response (OnionooInterface): Onionoo api response

        Returns:
            List[Relay]: List of relays from the response
        """
        deserialised_relays = []
        for relay in response.relays:
            raw_relay_obj = RawOnionooRelayInterface(**relay)  # type: ignore
            relay_obj = cls._parse_relay(raw_relay_obj)
            deserialised_relays.append(relay_obj)
        return deserialised_relays

    @classmethod
    def get_bridges(cls, response: OnionooInterface) -> List[OnionooBridgeInterface]:
        """Get list of all the bridges from the onionoo api call

        Args:
            response (OnionooInterface): Onionoo api response

        Returns:
            List[Bridge]: List of bridges from the response
        """
        deserialised_bridges = []
        for bridge in response.bridges:
            raw_bridge_obj = RawOnionooBridgeInterface(**bridge)  # type: ignore
            bridge_obj = cls._parse_bridge(raw_bridge_obj)
            deserialised_bridges.append(bridge_obj)
            pass
        return deserialised_bridges
