from celery import shared_task

from tor_weather.src.service.subscription import SubscriptionSvc
from tor_weather.tasks.onionoo.api import OnionooApi


@shared_task()
def onionoo() -> None:
    response = OnionooApi().get_response()
    relays = OnionooApi().get_relays(response)
    bridges = OnionooApi().get_bridges(response)
    for relay in relays:
        SubscriptionSvc.validate_node(relay, is_bridge=False)
    for bridge in bridges:
        SubscriptionSvc.validate_node(bridge, is_bridge=True)
