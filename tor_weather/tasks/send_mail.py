from smtplib import SMTPException
from typing import Any

from celery import shared_task
from flask_mailman import EmailMultiAlternatives

from tor_weather.extensions import task_logger
from tor_weather.src.dto.mail import MailDTO


@shared_task()
def send_mail(content: dict[str, Any], retry_count=3) -> None:
    """Send email

    Args:
        content (MailDTO): Content for the email
        retry_count (int, optional): Available retry count. Defaults to 3.
    """
    try:
        if retry_count > 0:
            mail_content = MailDTO(**content)
            message = EmailMultiAlternatives(
                subject=mail_content.subject,
                body=mail_content.text_body,
                from_email=mail_content.from_email,
                to=mail_content.to,
            )
            message.attach_alternative(mail_content.html_body, "text/html")
            message.send()
    except SMTPException as e:
        task_logger.error("Failed to send an email at attempt %s - %s", retry_count, e)
        send_mail.delay(content, retry_count - 1)  # type: ignore
