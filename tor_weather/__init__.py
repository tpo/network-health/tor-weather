from flask import Flask, redirect
from flask_migrate import Migrate
from flask_restx import Api
from werkzeug import Response

from tor_weather.env import load_env_to_app_config
from tor_weather.error import APIError
from tor_weather.src.constants.common import MessageResponseTypes
from tor_weather.src.dto.response import TemplateResponseDTO
from tor_weather.utilities.response import RedirectResponse, TemplateResponse

from .routes import configure_namespaces, register_routes


def create_app() -> Flask:
    """Creates & Intialises the Flask Backend

    Returns:
        Flask: Instance of Flask App
    """
    app: Flask = _get_app()
    load_env_to_app_config(app)
    configure_home_route(app)
    configure_database_and_migrations(app)
    configure_mail(app)
    configure_celery(app)
    configure_celery_beat(app)
    configure_login_manager(app)
    configure_routes(app)
    return app


def _get_app() -> Flask:
    """Creates an app instance & returns it

    Returns:
        Flask: Instance of Flask App
    """
    app: Flask = Flask(__name__, template_folder="./templates")
    return app


def configure_database_and_migrations(app: Flask) -> None:
    """Configures the database instance with the tables

    Args:
        app (Flask): The current app instance
    """
    from .extensions import db

    db.init_app(app)
    Migrate(app, db)


def configure_mail(app: Flask) -> None:
    """Configures mail

    Args:
        app (Flask): The current app instance
    """
    from .extensions import mail

    mail.init_app(app)


def configure_celery(app: Flask) -> None:
    """Configures celery

    Args:
        app (Flask): The current app instance
    """
    from .extensions import celery

    celery.conf.update({"broker_url": app.config["BROKER_URL"]})

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.set_default()
    celery.Task = ContextTask


def configure_celery_beat(app: Flask) -> None:
    """Configures celery beat

    Args:
        app (Flask): The current app instance
    """

    from .extensions import celery

    celery.conf.update(
        {
            "beat_schedule": {
                "onionoo": {
                    "task": "tor_weather.tasks.onionoo.onionoo",
                    "schedule": 60 * int(app.config["ONIONOO_JOB_INTERVAL"]),
                }
            }
        }
    )


def configure_login_manager(app: Flask) -> None:
    """Configures the login manager

    Args:
        app (Flask): The current app instance
    """
    from .extensions import login_manager

    login_manager.login_view = "login"  # type: ignore
    login_manager.login_message_category = MessageResponseTypes.ERROR.value
    login_manager.init_app(app)

    from tor_weather.database.subscriber import Subscriber

    @login_manager.user_loader
    def load_user(user_id):
        return Subscriber.query.get(int(user_id))


def configure_home_route(app: Flask) -> None:
    """Configures the home route for the app

    Args:
        app (Flask): The current app instance
    """

    @app.get("/")
    def home() -> Response:
        return redirect("/login")


def configure_routes(app: Flask) -> None:
    """Configures the routes

    Args:
        app (Flask): The current app instance
    """
    from .extensions import api

    api.init_app(app)
    configure_namespaces(api)
    register_routes()
    register_error_routes(app, api)


def register_error_routes(app: Flask, api: Api) -> None:
    """
    Configures the error routes for the app
    """

    @app.errorhandler(APIError)  # type: ignore
    def handle_error(error: APIError) -> Response:
        return RedirectResponse(error.get_response()).to_response()

    @app.errorhandler(404)
    def handle_not_found(error) -> Response:
        payload = TemplateResponseDTO(template_url="/pages/error/404.html")
        return TemplateResponse(payload).to_response()
