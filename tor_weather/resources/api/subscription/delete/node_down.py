from typing import cast

from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.database.subscriber import Subscriber
from tor_weather.routes import api_ns
from tor_weather.src.service.subscriptions.node_down import NodeDownSubscriptionSvc
from tor_weather.utilities.response import RedirectResponse


class NodeDownSubscriptionDeleteApi(Resource):
    """Implements the Node Down Subscription Delete API"""

    @login_required
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def get(self, fingerprint: str):
        subscriber = cast(Subscriber, current_user)
        payload = NodeDownSubscriptionSvc.delete(subscriber.email, fingerprint)
        response = RedirectResponse(payload)
        return response.to_response()
