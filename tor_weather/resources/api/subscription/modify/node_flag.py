from typing import cast

from flask import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.database.subscriber import Subscriber
from tor_weather.routes import api_ns
from tor_weather.src.dto.subcriptions.node_flag import NodeFlagSubDTO
from tor_weather.src.service.subscriptions.node_flag import NodeFlagSubscriptionSvc
from tor_weather.utilities.response import RedirectResponse

request_parser = api_ns().parser()
request_parser.add_argument(
    "fingerprint",
    type=str,
    help="Fingerprint of the node to be modified",
    required=True,
    location="form",
)
request_parser.add_argument(
    "flag",
    type=str,
    help="Name of the flag to subscribe for",
    required=True,
    location="form",
)
request_parser.add_argument(
    "wait_for",
    type=float,
    help="Time to wait before sending an email",
    required=True,
    location="form",
)


class NodeFlagSubscriptionModifyApi(Resource):
    """Implements the Node Flag Subscription Modify API"""

    @login_required
    @api_ns().doc(parser=request_parser)
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def post(self) -> Response:
        subscriber = cast(Subscriber, current_user)
        args = request_parser.parse_args()
        payload = NodeFlagSubscriptionSvc.modify(
            email=subscriber.email, node=NodeFlagSubDTO(**args)
        )
        response = RedirectResponse(payload)
        return response.to_response()
