from typing import cast

from flask import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.database.subscriber import Subscriber
from tor_weather.routes import api_ns
from tor_weather.src.service.subscriptions.node_down import NodeDownSubscriptionSvc
from tor_weather.utilities.response import RedirectResponse


class NodeDownSubscriptionDisableApi(Resource):
    """Implements the Node Down Subscription Disable API"""

    @login_required
    @api_ns().response(302, "Redirect to the Subscription List Page")
    def get(self, fingerprint: str) -> Response:
        subscriber = cast(Subscriber, current_user)
        payload = NodeDownSubscriptionSvc.disable(subscriber.email, fingerprint)
        response = RedirectResponse(payload)
        return response.to_response()
