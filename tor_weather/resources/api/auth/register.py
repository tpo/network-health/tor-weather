from flask import Response
from flask_restx import Resource

from tor_weather.routes import api_ns
from tor_weather.src.service.subscriber import SubscriberSvc
from tor_weather.utilities.response import RedirectResponse

register_parser = api_ns().parser()
register_parser.add_argument("email", type=str, location="form", required=True)
register_parser.add_argument("password", type=str, location="form", required=True)


class RegisterApi(Resource):
    """Implements API for Register"""

    @api_ns().doc(parser=register_parser)
    @api_ns().response(302, "Redirect to the Login or Email Verification Page")
    @api_ns().response(400, "Incorrect Input")
    def post(self) -> Response:
        args = register_parser.parse_args()
        payload = SubscriberSvc.register(**args)
        response = RedirectResponse(payload)
        return response.to_response()
