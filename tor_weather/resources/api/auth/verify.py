from flask import request
from flask.wrappers import Response
from flask_restx import Resource

from tor_weather.routes import api_ns
from tor_weather.src.service.subscriber import SubscriberSvc
from tor_weather.utilities.response import RedirectResponse

verify_parser = api_ns().parser()
verify_parser.add_argument("code", type=str, location="args", required=True)


class VerifyApi(Resource):
    """Implements API for Verification"""

    @api_ns().response(302, "Redirect to Login or Incorrect Credentials")
    def get(self) -> Response:
        verification_code: str = request.args["code"]
        payload = SubscriberSvc.verify(verification_code)
        response = RedirectResponse(payload)
        return response.to_response()
