from flask.wrappers import Response
from flask_restx import Resource, fields

from tor_weather.routes import api_ns
from tor_weather.src.service.subscriber import SubscriberSvc
from tor_weather.utilities.response import RedirectResponse

login_parser = api_ns().parser()
login_parser.add_argument("email", type=str, location="form", required=True)
login_parser.add_argument("password", type=str, location="form", required=True)

login_model = api_ns().model("login_model", {"something": fields.String("something")})

response_login_model = api_ns().model(
    "response_login_model",
    {
        "payload": fields.Nested(login_model),
        "message": fields.String(description="status of the api call", enum=["success", "failure"]),
        "status": fields.Integer(description="http response code"),
    },
)


class LoginApi(Resource):
    """Implements API for Login"""

    @api_ns().doc(parser=login_parser)
    @api_ns().response(302, "Redirect to Dashboard or Login Screen")
    @api_ns().response(400, "Incorrect Input")
    # @api_ns.doc(
    #     params={},
    #     response={200: "Success", 400: "Validation Error"},
    #     description="",
    #     model=response_login_model,
    # )
    def post(self) -> Response:
        args = login_parser.parse_args()
        payload = SubscriberSvc.login(**args)
        response = RedirectResponse(payload)
        return response.to_response()
