from flask import Response
from flask_restx import Resource

from tor_weather.routes import api_ns
from tor_weather.src.service.subscriber import SubscriberSvc
from tor_weather.utilities.response import RedirectResponse


class LogoutApi(Resource):
    """Implements all API's related to Logout"""

    @api_ns().response(302, "Redirect to the Login Screen")
    def get(self) -> Response:
        payload = SubscriberSvc.logout()
        response = RedirectResponse(payload)
        return response.to_response()
