from flask import url_for
from flask.wrappers import Response
from flask_login import login_required
from flask_restx import Resource

from tor_weather.routes import template_ns
from tor_weather.src.dto.response import RedirectResponseDTO
from tor_weather.utilities.response import RedirectResponse


class DashboardHomeTemplate(Resource):
    """Implements the Dashboard's Home Page"""

    @login_required
    @template_ns().response(301, "Redirect to the current home page for dashboard")
    def get(self) -> Response:
        payload = RedirectResponseDTO(redirect_url=url_for("node_down_list"))
        response = RedirectResponse(payload)
        return response.to_response()
