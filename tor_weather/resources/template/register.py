from flask import Response
from flask_restx import Resource

from tor_weather.extensions import app_logger
from tor_weather.routes import template_ns
from tor_weather.src.dto.response import TemplateResponseDTO
from tor_weather.utilities.response import TemplateResponse


class RegisterTemplate(Resource):
    """Implements the Register Page"""

    @template_ns().response(200, "Success")
    def get(self) -> Response:
        app_logger.info("Register Template Requested")
        payload = TemplateResponseDTO(template_url="pages/register.html")
        response = TemplateResponse(payload)
        return response.to_response()
