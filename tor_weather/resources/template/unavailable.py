from flask import request
from flask.wrappers import Response
from flask_login import login_required
from flask_restx import Resource

from tor_weather.routes import template_ns
from tor_weather.src.constants.sidebar import PAGE
from tor_weather.src.dto.response import TemplateResponseDTO
from tor_weather.src.service.presentation.breadcrumb import BreadcrumbSvc
from tor_weather.src.service.presentation.sidebar import SidebarSvc
from tor_weather.utilities.response import TemplateResponse


class ComingSoonTemplate(Resource):
    """Implements the Coming Soon Page"""

    @staticmethod
    def get_page(path: str) -> PAGE:
        if path == "/dashboard/node-status/node-version":
            return PAGE.NODE_VERSION
        if path == "/dashboard/events/earned-tshirt":
            return PAGE.CLAIM_TSHIRT_AWARD
        if path == "/dashboard/events/monthly-stats":
            return PAGE.MONTHLY_STATS
        if path == "/dashboard/events/relay-operators-meetup":
            return PAGE.RELAY_OPERATORS_MEETUP
        if path == "/dashboard/events/leaderboard":
            return PAGE.LEADERBOARD
        return PAGE.NODE_DOWN

    @login_required
    @template_ns().response(200, "Success")
    def get(self) -> Response:
        page = self.get_page(request.path)

        data = {
            "sidebar": SidebarSvc.get_data(page),
            "breadcrumb": BreadcrumbSvc.get_data(page),
            "coming_soon": True,
        }
        payload = TemplateResponseDTO("/pages/dashboard/coming_soon.html", data=data)
        response = TemplateResponse(payload)
        return response.to_response()
