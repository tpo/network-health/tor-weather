from flask.wrappers import Response
from flask_login import login_required
from flask_restx import Resource

from tor_weather.routes import template_ns
from tor_weather.src.constants.breadcrumb import BREADCRUMB_LEVEL
from tor_weather.src.constants.sidebar import PAGE
from tor_weather.src.dto.response import TemplateResponseDTO
from tor_weather.src.service.presentation.breadcrumb import BreadcrumbSvc
from tor_weather.src.service.presentation.form import FormSvc
from tor_weather.src.service.presentation.info_card import InfoCardSvc
from tor_weather.src.service.presentation.sidebar import SidebarSvc
from tor_weather.utilities.response import TemplateResponse


class NodeFlagCreateTemplate(Resource):
    """Implements the Node-Flag Subscription Create Page"""

    subscription_category: str = "node-status"
    subscription_name: str = "node-flag"
    form_fields: list[str] = ["fingerprint", "wait_for"]

    @login_required
    @template_ns().response(200, "Success")
    def get(self) -> Response:
        data = {
            "sidebar": SidebarSvc.get_data(PAGE.NODE_FLAG),
            "breadcrumb": BreadcrumbSvc.get_data(PAGE.NODE_FLAG, [BREADCRUMB_LEVEL.CREATE]),
            "informationCard": InfoCardSvc.get_node_flag_cards(),
            "subscriptionForm": FormSvc.get_node_flag_create_form(),
        }
        payload = TemplateResponseDTO("/pages/dashboard/subscription-edit-create.html", data=data)
        response = TemplateResponse(payload)
        return response.to_response()
