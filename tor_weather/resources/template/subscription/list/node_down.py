from typing import cast

from flask import url_for
from flask.wrappers import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.database.subscriber import Subscriber
from tor_weather.routes import template_ns
from tor_weather.src.constants.sidebar import PAGE
from tor_weather.src.dto.response import TemplateResponseDTO
from tor_weather.src.service.presentation.breadcrumb import BreadcrumbSvc
from tor_weather.src.service.presentation.sidebar import SidebarSvc
from tor_weather.src.service.subscriptions.node_down import NodeDownSubscriptionSvc
from tor_weather.utilities.response import TemplateResponse


class NodeDownListTemplate(Resource):
    """Implements the Node-Down Subscription List Page"""

    @login_required
    @template_ns().response(200, "Success")
    def get(self) -> Response:
        subscriber = cast(Subscriber, current_user)

        data = {
            "sidebar": SidebarSvc.get_data(PAGE.NODE_DOWN),
            "breadcrumb": BreadcrumbSvc.get_data(PAGE.NODE_DOWN),
            "table": NodeDownSubscriptionSvc.get_subscriptions(subscriber.email),
            "createUrl": url_for("node_down_create"),
        }
        payload = TemplateResponseDTO("/pages/dashboard/subscription-list.html", data=data)
        response = TemplateResponse(payload)
        return response.to_response()
