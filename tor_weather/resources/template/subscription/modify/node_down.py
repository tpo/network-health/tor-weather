from typing import cast

from flask.wrappers import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.database.subscriber import Subscriber
from tor_weather.routes import template_ns
from tor_weather.src.constants.breadcrumb import BREADCRUMB_LEVEL
from tor_weather.src.constants.sidebar import PAGE
from tor_weather.src.dto.response import TemplateResponseDTO
from tor_weather.src.service.presentation.breadcrumb import BreadcrumbSvc
from tor_weather.src.service.presentation.form import FormSvc
from tor_weather.src.service.presentation.info_card import InfoCardSvc
from tor_weather.src.service.presentation.sidebar import SidebarSvc
from tor_weather.src.service.subscriptions.node_down import NodeDownSubscriptionSvc
from tor_weather.utilities.response import TemplateResponse


class NodeDownModifyTemplate(Resource):
    """Implements the Node-Down Subscription Modify Page"""

    @login_required
    @template_ns().response(200, "Success")
    def get(self, fingerprint: str) -> Response:
        subscriber = cast(Subscriber, current_user)
        data = {
            "sidebar": SidebarSvc.get_data(PAGE.NODE_DOWN),
            "breadcrumb": BreadcrumbSvc.get_data(PAGE.NODE_DOWN, [BREADCRUMB_LEVEL.MODIFY]),
            "informationCard": InfoCardSvc.get_node_down_cards(),
            "subscriptionForm": FormSvc.get_node_down_modify_form(
                default=NodeDownSubscriptionSvc.get_subscription(
                    email=subscriber.email, fingerprint=fingerprint
                )
            ),
        }
        payload = TemplateResponseDTO("/pages/dashboard/subscription-edit-create.html", data=data)
        response = TemplateResponse(payload)
        return response.to_response()
