from flask.wrappers import Response
from flask_restx import Resource

from tor_weather.routes import template_ns
from tor_weather.src.dto.response import TemplateResponseDTO
from tor_weather.utilities.response import TemplateResponse


class LoginTemplate(Resource):
    """Implements the Login Page"""

    @template_ns().response(200, "Success")
    def get(self) -> Response:
        payload = TemplateResponseDTO(template_url="pages/login.html")
        response = TemplateResponse(payload)
        return response.to_response()
