#!/bin/bash

echo Load Environment Variables
echo "set -o allexport" >> ~/.bashrc
echo "source ./deployment/dev/.env" >> ~/.bashrc
echo "set +o allexport" >> ~/.bashrc
source ~/.bashrc

echo Install precommit hooks
pre-commit install

echo Initiate database migration
poetry run flask db upgrade --directory ./tor_weather/migrations

/bin/bash