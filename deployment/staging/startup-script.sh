#!/bin/bash

echo Load Environment Variables
echo "set -o allexport" >> ~/.bashrc
echo "source ./deployment/staging/.env" >> ~/.bashrc
echo "set +o allexport" >> ~/.bashrc
source ~/.bashrc

echo Initiate database migration
poetry run flask db upgrade --directory ./tor_weather/migrations

echo Start server
poe dev-prod
