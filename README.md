## Introduction

Tor-weather is a notification service which helps the node operators get notified when some incident happens with their relays or bridges. The service is intended to have 2 major functions:

To provide notification on possible incidents with the nodes.
To incentivise node operators for the work. (PENDING)

## Usage

1. Node operators should visit the web portal for tor-weather & register themselves.
2. Registration is minimal, requires an email-id & a password.
3. A verification-email will be sent, which will be used to validate the account.
4. Upon logging in the portal, users can subscribe to different types of notifications for nodes.

## Technical Specifications

The project is built up using `Flask`. It uses `Postgresql` as database in the production environment, and heavily relies on `poetry` for easing out the process of setup & deployments.

## Dev Setup

### 1. Using Docker Compose (Recommended)
Development setup can be done with the help of docker-compose. This is ideally the easiest way to setup the repository on your local system.

#### If you are using VS-Code
1. Install `docker` & `docker-compose` in your system if not already present.
2. Install `Dev Containers (ms-vscode-remote.remote-containers)` extension if not already present in your VS-Code.
3. Click on `Reopen in Container` button in the popup on the bottom right corner. If the pop-up doesn't appear, you can navigate from `Ctrl+Shift+P` menu.
4. VS-Code will automatically build all the containers, and will reload the window in the `app-server` container. The command line will connect you with the `app-server` container.
5. Inside the `app-server` container, run the `poe dev-local` to start the development server.
6. Further options are easily accessible from `Remote Explorer` tab in VS-Code. This can help you view logs, rebuild containers, etc.

#### If you are not using VS-Code
1. Install `docker` & `docker-compose` in your system if not already present.
2. Run `docker-compose up` in deployment/dev.
3. Open a shell inside the `tor_weather_app_server` container with `docker exec -it tor_weather_app_server bash`
4. Inside the `tor_weather_app_server` container, run `poe dev-local` to start the development server.

#### Services available as part of docker-compose

1. **App-Server (port:3000)** - This runs the application server.
    - User ID - *user@torproject.org*
    - Password - *password*
    - WebPage IP - *localhost:3000*

2. **Postgres (port:5432)** - This runs the postgres db.
    - User ID - *user@torproject.org*
    - Password - *password*
    - Database - *tor_weather*

4. **MailCatcher (port:1080, 1025)** - This provides a service to view the emails sent from the application server.
    - User ID - *user@torproject.org*
    - Password - *password*
    - WebPage IP - *localhost:1080*


### 2. Manual Setup (Legacy)
Development setup is possible by installing all the services locally & establishing communication channels for them to interact. Depending on your exposure to such technologies, this can range from complicated to very complicated, while also being subjected to changes with time.

1. Install `python`, `poetry`, `postgresql`, `poethepoet`.
2. Update the database connection strings in the `.env` file.
3. Update the smtp connection strings in the `.env` file.
    1. If you are using credentials of your personal email, make sure you don't push these. (`.env` file is not a part of `.gitignore`)
    2. You can install tools like `mailcatcher` & use them to catch the emails. (Recommended)
4. Run `poetry run flask db upgrade` to initiate database migrations.
5. Run `poe seed` to add mock data.
6. Install `pre-commit` on your system & run `pre-commit install` to make sure your changes are automatically formatted & linted.
4. Run `poe dev-local` for starting the local server.


## Prod Setup

1. Run `poetry install`
2. Create a `.env` file on the root of the project & add the required variables. (Check `src/settings.py` for the required variables)
3. Make sure `FLASK_ENV` is set to `production` in environment variables.
4. Build the wheel using `poetry build --format wheel`
5. Install the wheel using `python3 -m pip install dist/tor_weather-*.whl --force-reinstall`
6. Run `python3 -m tor_weather.job` for running the job.
7. Run `python3 -m gunicorn -w 1 -b localhost:5000 tor_weather.app:app` for running the backend.

## Data Migration

1. Data migrations are enabled with the use of `alembic`.
2. Execution of the migration scripts can be done from flask wrapper, i.e. by `flask db upgrade`, or by directly pointing alembic to `alembic.ini` file & executing `alembic upgrade head --config <path_to_alembic.ini>`.
3. To create a new migration, update the models for the tables & run `flask db migrate -m <migration-message>`.
4. This will create a new migration file under the `migrations/versions` directory.
5. Check the file manually as alembic can leave out cases which are better handled manually.

## Increment Version

1. Run `poe increment-version` to automatically identify the next tag, and update the `CHANGELOG.md`.
2. Commit the changes, preferably with the tag prepended in `CHANGELOG.md`.
3. Create a new tag associated with the current commit, `git tag -m "<version>" -a <version> -s`.
4. Push the commit & tag associated with it, `git push --follow-tags`.
5. Upon pushing a tag, a pipeline will automatically trigger & will prepare a release.

## Notes

1. Prod setup might require creating a `logs` directory in the cwd.
2. Dev Setup using `docker-compose` exposes the `app-server` at host `0.0.0.0`. You might want to limit access from everyone on the network with your firewall rules.
