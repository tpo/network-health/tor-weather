import json
import os

import sqlalchemy as db
from dotenv import load_dotenv
from sqlalchemy.orm import Session

from tor_weather.database.relay import Relay
from tor_weather.database.subscriber import Subscriber
from tor_weather.database.subscription import Subscription
from tor_weather.database.subscriptions.node_bandwidth_sub import NodeBandwidthSub
from tor_weather.database.subscriptions.node_down_sub import NodeDownSub
from tor_weather.database.subscriptions.node_flag.exit_sub import NodeFlagExitSub
from tor_weather.database.subscriptions.node_flag.fast_sub import NodeFlagFastSub
from tor_weather.database.subscriptions.node_flag.guard_sub import NodeFlagGuardSub
from tor_weather.database.subscriptions.node_flag.stable_sub import NodeFlagStableSub


def get_db_session() -> Session:
    """Get the database session

    Raises:
        AssertionError: Fails if sqlalchemy database uri is not configured in environment

    Returns:
        Session: Database session instance
    """
    db_uri = os.environ.get("SQLALCHEMY_DATABASE_URI")
    if db_uri:
        engine = db.create_engine(db_uri)
        return Session(engine)
    else:
        raise AssertionError("SQLALCHEMY_DATABASE_URI environment variable not set")


def seed_data_from_json(session: Session, file_path: str, model_class) -> None:
    """Seed data from json file into the database

    Args:
        session (Session): Database session instance
        file_path (str): Relative path to the JSON file
        model_class (unknown): Database table for which data is to be seeded
    """
    with open(file_path, "r") as json_file:
        data = json.load(json_file)

    for item in data["payload"]:
        record = model_class(**item)
        session.add(record)


def initiate_data_seed() -> None:
    """Initialised the data seeding for all tables"""
    seed_data_from_json(db_session, "./mock/database/subscribers.json", Subscriber)
    seed_data_from_json(db_session, "./mock/database/relays.json", Relay)
    seed_data_from_json(db_session, "./mock/database/subscriptions.json", Subscription)
    # Seed individual subscription data
    seed_data_from_json(db_session, "./mock/database/node_bandwidth_sub.json", NodeBandwidthSub)
    seed_data_from_json(db_session, "./mock/database/node_down_sub.json", NodeDownSub)
    seed_data_from_json(db_session, "./mock/database/node_flag_exit_sub.json", NodeFlagExitSub)
    seed_data_from_json(db_session, "./mock/database/node_flag_fast_sub.json", NodeFlagFastSub)
    seed_data_from_json(db_session, "./mock/database/node_flag_guard_sub.json", NodeFlagGuardSub)
    seed_data_from_json(db_session, "./mock/database/node_flag_stable_sub.json", NodeFlagStableSub)


if __name__ == "__main__":
    load_dotenv()
    db_session = get_db_session()
    if not db_session.query(Subscriber).count():
        print("Database doesn't contain any data, hence starting seeding operation")
        initiate_data_seed()
        db_session.commit()
    else:
        print("Database already contains data, hence skipping seeding operation.")
    db_session.close()
