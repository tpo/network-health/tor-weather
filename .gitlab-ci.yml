variables:
  PYTHON_VERSION: "3.11"

# Defining the stages in the Pipeline
stages:
  - Inspection
  - Build
  - Verify
  - Deploy
  - Release

# Rules
.pr_to_main:
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main"

.pr_to_dev:
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "dev"

.commit_to_dev:
  rules:
    - if: $CI_COMMIT_BRANCH == "dev"

.tag_release:
  rules:
    - if: $CI_COMMIT_TAG

# Section for Inspection
.install_poetry:
  image: python:$PYTHON_VERSION
  before_script:
    - "apt-get update"
    - "python3 -m pip install --upgrade pip"
    - "python3 -m pip install --user pipx"
    - "python3 -m pipx ensurepath"
    - export PATH=$PATH:/root/.local/bin
    - "pipx install poetry"
    - "pipx install poethepoet"

black:
  extends: .install_poetry
  stage: Inspection
  rules:
    - !reference [.pr_to_main, rules]
    - !reference [.pr_to_dev, rules]
  script:
    - poetry install --only dev
    - poe black-all-check

flake8:
  extends: .install_poetry
  stage: Inspection
  rules:
    - !reference [.pr_to_main, rules]
    - !reference [.pr_to_dev, rules]
  script:
    - poetry install --only dev
    - poe flake8-all

bandit:
  extends: .install_poetry
  stage: Inspection
  rules:
    - !reference [.pr_to_main, rules]
    - !reference [.pr_to_dev, rules]
  script:
    - poetry install --only dev
    - poe bandit-all

isort:
  extends: .install_poetry
  stage: Inspection
  rules:
    - !reference [.pr_to_main, rules]
    - !reference [.pr_to_dev, rules]
  script:
    - poetry install --only dev
    - poe isort-all-check

djlint-formatting:
  extends: .install_poetry
  stage: Inspection
  rules:
    - !reference [.pr_to_main, rules]
    - !reference [.pr_to_dev, rules]
  script:
    - poetry install --only dev
    - poe djlint-all-check

djlint-linting:
  extends: .install_poetry
  stage: Inspection
  rules:
    - !reference [.pr_to_main, rules]
    - !reference [.pr_to_dev, rules]
  script:
    - poetry install --only dev
    - poe djlint-all-lint

mypy:
  extends: .install_poetry
  stage: Inspection
  rules:
    - !reference [.pr_to_main, rules]
    - !reference [.pr_to_dev, rules]
  script:
    - poetry install
    - poe mypy

# Section for Build Stage

build:
  extends: .install_poetry
  stage: Build
  rules:
    - !reference [.pr_to_main, rules]
    - !reference [.tag_release, rules]
    - !reference [.pr_to_dev, rules]
  environment:
    name: Development
  script:
    - poetry version ${CI_COMMIT_TAG}
    - poetry build
  artifacts:
    when: on_success
    expire_in: "60 mins"
    paths:
      - "dist/*.whl"

# Section for Verify Stage
.install_wheel:
  image: python:$PYTHON_VERSION
  variables:
    GIT_STRATEGY: none
  before_script:
    - "apt-get update"
    - "python3 -m pip install --upgrade pip"
    - "python3 -m pip install dist/tor_weather-*.whl"

# Section for Deploy Stage
deploy:
  stage: "Deploy"
  image: ubuntu:latest
  environment:
    name: Staging
  rules:
    - !reference [.commit_to_dev, rules]
  before_script:
    - apt-get -yq update
    - apt-get -yqq install ssh
    - install -m 600 -D /dev/null ~/.ssh/id_rsa
    - echo $SSH_PRIVATE_KEY | base64 -d > ~/.ssh/id_rsa
    - ssh-keyscan -H $SSH_HOST > ~/.ssh/known_hosts
  script:
    - ssh -v $SSH_USER@$SSH_HOST "cd tor-weather && git fetch --all && git checkout dev && git pull && docker compose -f ./deployment/staging/docker-compose.yaml down && docker image prune -af && docker compose -f ./deployment/staging/docker-compose.yaml up -d"
  after_script:
    - rm -rf ~/.ssh

# Section for Release Stage
upload-to-registry:
  stage: Release
  image: python:$PYTHON_VERSION
  environment:
    name: Production
  rules:
    - !reference [.tag_release, rules]
  script:
    - python3 -m pip install twine
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python3 -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*

release:
  stage: Release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - !reference [.tag_release, rules]
  script:
    - cp dist/*.whl tor_weather_${CI_COMMIT_TAG}.whl
  environment:
    name: Production
  artifacts:
    when: on_success
    expire_in: "never"
    name: "tor_weather_${CI_COMMIT_TAG}"
    paths:
      - tor_weather_${CI_COMMIT_TAG}.whl
  release:
    name: "Tor-Weather v${CI_COMMIT_TAG}"
    tag_name: "$CI_COMMIT_TAG"
    description: "Tor-Weather v${CI_COMMIT_TAG}"
    assets:
      links:
        - name: "Tor-Weather Wheel"
          url: "https://gitlab.torproject.org/tpo/network-health/tor-weather/-/jobs/${CI_JOB_ID}/artifacts/raw/tor_weather_${CI_COMMIT_TAG}.whl"
