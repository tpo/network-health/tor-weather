import json
import os

from flask import Flask

app = Flask("mock-server")


def read_json(file_name):
    dir_path = os.path.dirname(__file__)
    file_path = os.path.join(dir_path, file_name)
    f = open(file_path)
    data = json.load(f)
    f.close()
    return data


@app.route("/details")
def details():
    return read_json("./response/details.json")


app.run(port=3000)
