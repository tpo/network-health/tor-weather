# Changelog

## [2.1.0] - 2024-05-30

### Features

- updated links for get-help & submit-feedback sidebar options - ([b557275](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b55727566a38a21c2c4cd3b12778694b2262be1e))


### Refactoring

- migrated from scss to css - ([2f0411b](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/2f0411b1b67f33f08cf72b2ad467f878d9f15bf9))



## [2.0.3] - 2024-04-25

### Bug Fixes

- sidebar text overflow on small screen-sizes - ([3077a35](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3077a35ca6825c26ff6d409ee07161c42df938bb))


### Miscellaneous Chores

- upgraded python to 3.11 - ([e7d8e82](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/e7d8e82a734d97a0b9ef0975e8f4b13df43d764c))



## [2.0.2] - 2024-03-25

### Bug Fixes

- incorrect emails sent by onionoo job - ([40d191e](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/40d191ec092bb50bb8ffe502fd08765afd57b5d9))



## [2.0.1] - 2024-03-22

### Bug Fixes

- onionoo job failure due to optional 'as' keyword - ([5b874b9](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/5b874b9afec9d4a82fe63db8a05f973b38d00d3d))

- migration f9dcbbd2d19b with default value for is_bridge - ([08ba526](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/08ba5269b84c2b3272ecc6a8ba93eff7c449054e))

- Merge remote-tracking branch 'origin/merge-requests/207' into dev - ([b53cb0b](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b53cb0b75a15755d00e46c25dbde825b11a1d51a))


### Documentation

- updated tag-release readme to include signed commits - ([24263cf](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/24263cfa8135a143b4419af5974c5807f6033c59))


### Miscellaneous Chores

- Merge remote-tracking branch 'origin/merge-requests/202' into dev - ([b03c4c9](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b03c4c91cf03ed3363f77b4fb214c0a5db793461))



## [2.0.0] - 2024-03-11

### Bug Fixes

- onionoo job crashing due to optional data - ([c3c7d78](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c3c7d785d1b3aec86e5f539aa208eeaeb5575e32))

- snackbar formatting & z-index of snackbar - ([3e42bf3](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3e42bf330541214563d0fda7e69f17d40e94c15b))

- locked dependency versions - ([9f1d11c](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/9f1d11c91500a980b3a9fa50f66d1d9cf099a7f5))

- docker-compose setup for macos systems - ([1566c55](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/1566c553f25f2a9e323fd24f8d52e8988911614e))

- prevent seeding operation when database contains data - ([b5639fd](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b5639fd3095a66394e5e8f27435c4e923aa5d7cb))

- called the missing seed method - ([85e35fc](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/85e35fcb570e111f3f2b65ef65cbf8203a2d2f5a))

- updated deployment command - ([ba60967](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/ba60967196e0d329ed6fa4110ebb982a0de68a55))

- added environments in gitlab ci & modified staging url's - ([6bca1e8](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/6bca1e844ecee22d326b654f16bb9d0e547355e9))

- stage name - ([2332aed](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/2332aed2f3c064f13492c0c02db963c9a14ef3c5))

- node-bandwidth create subscription - ([1f28d7c](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/1f28d7c67bfc228a5e0b02181d1bba25d87619d2))

- node-flag delete, enable, disable subscription - ([cc11e67](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/cc11e67b79af8658135877e9bf2281e8c7b946f2))

- djlint errors - ([e618040](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/e618040888b23908ca7d9c14368af40a0a5fe393))

- added error when duplicate subscription is added - ([595fbac](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/595fbac33800dc479a003764f76dd56765073773))

- create subscription & snackbars - ([3873f23](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3873f234c2dfa64c470a9258fd972b012272c4d3))

- code-review changes - ([7b1e6e2](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/7b1e6e2c1e814f41b004886d03f470e323fc53d1))

- removed jwt_secret from env variables - ([37f4237](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/37f423798840d2d83f8a11f209fd5749ec0f02af))

- removed depends - ([35fc56f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/35fc56fd7e94a49cdf5e3b4a43bb795113ecb66b))

- beat & worker scripts - ([32d216f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/32d216f96e144becf1b129f7c524e1fec3fb15f1))

- broker_url - ([e58defc](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/e58defcf9a58d9ef7bb2ca97b6c51645d71eb657))

- removed trailing slash from api_url - ([c9450b9](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c9450b9344604eb3fe8f2d3f7e841f1aeb6ec09d))

- keep only .gitkeep for logs directory - ([57699ba](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/57699baa2f08dd80cef695542b0fc22da4a57917))

- env variables for seeder & mock data - ([86c312f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/86c312f5c34eacdadb9f31c9db8d39b2ffaa0521))

- removed email unsubscription option - ([dba5513](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/dba5513f675df1ae869c56aed5d54ea11acdaf2d))


### Documentation

- added documentation related to data migrations - ([7445840](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/74458406bb800668ec1e92340b0c6c39992df703))

- updated readme with dev containers setup - ([e2a2433](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/e2a243314aa445520323a44367ab13b143ab7391))

- updated readme - ([c9b57fe](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c9b57fe052ac05a8152915a62814cf9c8980284e))

- added changelog - ([d42c63b](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/d42c63b68cb3ce2f7e1815df519b02dd5c0cb033))

- added previous version releases in changelog - ([d8ae36d](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/d8ae36d1a14fc2f969d312cce5bf4a800ca393e3))

- fixed version titles - ([cc7ad73](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/cc7ad73288d63fae6eef374af03cb810e74a3562))


### Features

- enabled database migrations - ([803c9a6](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/803c9a66f9832ac5f5fc60f1c1340a1d8809856a))

- working celery worker with rabbitmq integration - ([328e9d5](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/328e9d5e6cb02941f2f43eaecb48cf560431db64))

- configured celery beat - ([56f2ec8](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/56f2ec8c2912ea9576433c696f080415d36b59a3))

- aligned email templates - ([9ebf982](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/9ebf98285fc34e5b60339311853b7d4f42c78429))

- rehauled node bandwidth - ([6df1be1](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/6df1be1f1bba10c515829c38e83c9634601812b1))

- refactored the onionoo job - ([195e21f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/195e21f365fb3a6e6066d61ee034281e7c892e75))

- working bridges in onionoo job - ([acc197a](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/acc197aa7a3fe1a120832059cf631ee035ca8814))

- working new subscription list - ([2ee5ac0](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/2ee5ac013a4534f2f632a7d000edc66cca378bb7))

- added deactivate, activate, delete service methods - ([0a63b0d](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/0a63b0d6a11e5bc6e0366f18ecb4e7e8daeeafb5))

- migrated login, register, verify, logout to new code - ([3ebd4d4](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3ebd4d4d44fc042c60a025a8f138b7e7bb5ac723))

- migrated subscription creation to new code - ([dbb96c4](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/dbb96c4c4686772e753146af9fea424bb64ce822))

- migrated subscription enable, disable, delete to new code - ([f31f90a](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/f31f90a5a9c60fa8766ecf6c809617be29ec9905))

- migrated subscription modify to new code - ([b2fde95](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b2fde9550776e222cca3a53b8cc44d0a8f938ae0))

- added text-based emails - ([8812994](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/8812994e87fd08694493b243e0fbb58d232a508e))

- migrated responses to common class - ([00daade](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/00daade1b7fb34b28156b7a2a419680b96a16ff4))

- migrated node-down & node-bandwidth listing table to new code - ([9472209](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/9472209e6a8170b5fb747008e5731d1e1acc00a6))

- migrated sidebar to new code - ([91a6e70](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/91a6e70c70c5784b18fbb9a2de2ed52fe9cb9bc0))

- migrated breadcrumb to new code - ([0a755fa](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/0a755faff5f963e133272c8707f57db5f7d4ec09))

- migrated modify subscription & form to new code - ([cbf2297](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/cbf2297ac2c7d3fd26d66f1a5682f4e44fd9be53))

- updated lock file - ([e263275](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/e263275a33a71de8e16421ee698f6c4835f60b40))

- enabled mypy in precommit & pipeline - ([75ab791](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/75ab791b76872de20f260f7effd59cb50d02b62f))

- added sqlalchemy stubs - ([4228f42](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/4228f42c0ad0f6a9b3078a842cb88d8a894656b0))

- added coming-soon page - ([fa9b73c](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/fa9b73c36d976d98b5184db26fc07c2f5dea4d7e))

- updated license including material-icons-outlined - ([32d0c8c](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/32d0c8cdbf815b235d80940117ef4e9254734c3b))

- added back python-dotenv dependency - ([ef7542f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/ef7542f780fb60b97fbbf7f3a01d1ef2eb3c9a6f))

- added celery-worker & celery-beat containers - ([b5fe866](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b5fe866618c744065391b08028dd617c54d84404))

- excluded commits from renovate-bot in changelog - ([a71c0c2](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/a71c0c2820db4af7a5f56de54488178b3cb6d1ba))


### Miscellaneous Chores

- cleaned up extra lines of code - ([16cf472](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/16cf4722ac4a85a5684aea4545afeac0a9f87fd5))

- initiated dev-containers - ([d587136](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/d5871361463b5bc545679dc84cf72601d8666dea))

- added container config for database - ([7c3396a](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/7c3396ab33bbe76e9c875b0aa1487699a0246cf3))

- finished setup for docker dev containers - ([14f3030](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/14f30300814d5dd15b23c7ec21929074939f30f5))

- undone changes to vscode's settings.json - ([4964252](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/4964252a3c0a7754b5e0ff1e3f373df68d6f6d9d))

- added container for mailcatcher & unignored env file - ([c54633b](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c54633bf6b913e707c9cca01c1aa6e27bc84f653))

- added pre-commit installation & customised extensions for devcontainer - ([3ee29c5](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3ee29c5ebd5a8adee7644d6439d9c08b7e0c2800))

- removed pgadmin service & added data-seeder service - ([dc78002](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/dc78002bab901ec9894f861ee811a4eb30b0f651))

- added seed-data script - ([75b6dd4](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/75b6dd4a443da9b233de890acbd97e8d519d0046))

- pulled in latest fix/cross-platform-setup - ([e14d9df](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/e14d9df35f4ee54f961a8a14e7c0d353a8b44e49))

- updated the startup script to enter into shell for app-server - ([dcf38b8](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/dcf38b8c08f4c8b2f8530b164150017ebb4bec42))

- added git & gpg credentials to app-server service - ([3c6c3e0](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3c6c3e029dfce2ae96ea94cbd712e9912599b9b8))

- undone changes in devcontainer.json - ([574bd2a](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/574bd2a3b2a1fdcf1f93c1e509f3e8328fd151a9))

- removed copy of gpg keys inside app_server container - ([f8f6a2b](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/f8f6a2bfbd605fdb56914db460f37347bbdc6516))

- removed vscode settings.json changes - ([57aaaef](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/57aaaefbb9a6630cf5aef44698f3addba05fb571))

- pulled in latest main - ([f345983](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/f345983f9db3edf33f52e7b7f383430a4632bcc8))

- done something for staging - ([b2b9064](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b2b90646d7b4291830ec278f267f0185cedfc332))

- moved migrations inside tor_weather directory - ([ce6640c](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/ce6640ca4f8f75d1859d3f25000d7a0a6cc97af1))

- working new dev setup - ([4307e76](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/4307e76673ba54183fc3e4cd129f42723497b80f))

- modified the database uri to refer the host system - ([805a11c](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/805a11cf5353b99b38dc37d971ca6d4dc95118f2))

- modified the email host to refer the host system - ([3a8e8a7](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3a8e8a75270f791739044d34053db1a25d7c3ccf))

- updated staging mailhog image - ([2b37d47](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/2b37d4730da703f35a5923e1bbab159a076f02be))

- updated the port exposed for mailhog - ([3a66f1d](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3a66f1d5a0d64041db321ccf5ed668c8c9072afe))

- started code refactor - ([5fa10e8](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/5fa10e8773a0503108374e28f0c1b6920714bd62))

- started code-overhaul - ([acb9bcd](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/acb9bcd10bdb0607fca984823c7f2e2026bed1de))

- integrated logger with time-based rollovers - ([7bb6b47](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/7bb6b47b015a1565ad497d4e5974da8f3f614bea))

- updated .gitignore for new log files - ([aa48a49](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/aa48a499c8e58e749bdaf26d0ac00f35fa5991b5))

- added missing files - ([1987024](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/19870241dc9033c626501cf9bd077ba5afe54dfa))

- install all packages for mypy check - ([a197ce9](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/a197ce92e0d56b6915952e610a567e63d5a26b7e))

- removed build & job check from pipeline - ([c255cd2](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c255cd2d5448d397f20ad9c37b98a657dc1e9945))

- removed old code - ([8793273](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/87932732916f4a4c4da39df0a286030a32395302))

- removed flake8 artifacts from pyproject.toml - ([5656e56](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/5656e566a653fb484e53b00a33377857fddbecc3))

- auto updated vscode settings - ([287abbd](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/287abbda75b3df56c9c2e6d5d77d826c8efcec31))

- updated onionoo_job_interval env variable - ([501dec4](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/501dec48ebdc36a128f9d5acc85c57fcf99740d4))

- added staging broker_url env variable - ([a0f8b21](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/a0f8b214fb1cbad2fbb98fc8bde50f9ff89a30e4))

- added docker-compose containers for celery - ([825bc75](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/825bc75af8d1f1cc68202e13509be362ecba81d0))

- updated smtp username password - ([6a3b1b6](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/6a3b1b682e19ac851771f1154a049b2a1275296a))

- updated localhost - ([f1dd8db](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/f1dd8db8042b65ccd0f6d0d2f7c044254543fb3b))

- added host systems - ([eccc589](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/eccc58991452e1820c7f0169cdbeaaaaeb5d3069))

- limited log file count to 30 - ([b26f902](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b26f902f9b35a4f966d14809118ea5c36772a9be))

- added check for conventional commits in pre-commit - ([6d48eb5](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/6d48eb5a3243c85a738cdacc01e4f82c753e55b7))

- added name for conventional-commit hook - ([037edf7](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/037edf78a95c06178853ad4b04dfb0e6c1e3c385))

- added increment-version script - ([9eebed5](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/9eebed5c98588124d3f0c455015856f948508e1a))


### Refactoring

- fixed formatting & linting issues - ([17cf810](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/17cf8102b35750a8a3a4450233179f24d7d27b64))

- added docs & refactored the seed into separate function - ([ef8cc92](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/ef8cc921325f07f7b42e13d33e217e631d09a999))


### Revert

- removed autoincrement from database models - ([7aa2bde](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/7aa2bdeb5b1b497a307e8bfa8264a7044b1e0978))


### Ci

- updated ci to upload to gitlab registry - ([c0016f4](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c0016f4431df1a6556eb2103dc36df9a67586c17))

- added command for upgrading version - ([10cf2f1](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/10cf2f182fed38cbc07600f1a278c1686c641ee5))

- removed repository flag from twine - ([7278815](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/72788159549a6ccd92adc304366057407abc2730))

- removed the pypirc dependency - ([a5f4499](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/a5f449916fd71c8abda477a489644444422fb5b9))

- copied upload exactly as from gitlab docs - ([52259a1](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/52259a168bb300d967c21bb980313015a9b9d8eb))

- cleaned ci file - ([26d2530](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/26d25300be41cd2131e0566dfe6f7ee3a08fa219))


### Cicd

- added deployment to staging server - ([73bd49e](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/73bd49eae6c54e5c22a6eb29f17138c989e5ca57))

- added script for execution on staging - ([41e1562](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/41e156215693f3f386f46cff5caa6b647e2daf8d))

- added more rules for dev branch - ([1b4f2d5](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/1b4f2d5b8212e572d5b865b30ce1b8e0369690be))

- updated staging - ([cc0f18f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/cc0f18f293d9399bb337100b586d821a94668e71))

- fixed dev & staging docker-compose - ([5b23b5f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/5b23b5ff799c3ab863259caa7380e6c4d3f0430a))



## [1.1.0] - 2023-05-25

### Bug Fixes

- isort command in pyproject - ([06219bb](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/06219bb448f1cba3f826e834e0f4262cbc37aa89))


### Features

- added self-hosting for icons - ([f64d003](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/f64d003f5f06f9b8629ba503ddede8ddcd893da8))

- added self-hosting for fonts - ([bdb0263](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/bdb0263a883d0817e61cb36a9c70f4216f464c37))

- added content-security-policy (csp) - ([d2159ce](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/d2159ce179abc757a5d6ffe6c33c2a277a786736))

- new ui for login & register pages - ([7b0c592](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/7b0c592e1c99364dc3d9ab8cf0874d483c917cb1))


### Miscellaneous Chores

- cleaned commands in pre-commit - ([c8566ad](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c8566adb92c100b1b448e654676b9ad5c7e70303))

- pulled in latest main - ([8f84de4](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/8f84de485eca211d930751c2811d5514966c3596))

- locked version of poetry to 1.4.0 in pipeline - ([ee7979a](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/ee7979a6cf2959b68d333203e8889e37d05bbc61))

- replaced with poethepoet - ([29d2b34](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/29d2b346d79b2f0ba6265b6a2e8fe2d2d7409e91))

- use poe command in pre-commit hooks for djlint - ([83c2515](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/83c2515ea67175f7cc34a5b09fad44435236c0f9))

- unlocked version for flask-login & djlint - ([5f5d433](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/5f5d43315335e6563f26ac44051a2cc571708151))


### Refactoring

- cleared extra line & whitespace - ([c7fc8b6](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c7fc8b6888202d767070f0ed10250e94d02f81e4))


### Ci

- removed makefile & migrated scripts to pyproject - ([7c6e1f9](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/7c6e1f94b0de585239d9a6bbe124659759d086be))



## [1.0.0] - 2023-03-29

### Bug Fixes

- prevent negative inputs for subscriptions & add port config from env variables - ([aa52215](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/aa5221547874c120a4f35281916024b9bd34c7d8))



## [0.0.1] - 2023-03-23

### Bug Fixes

- undone the changes done by isort in __init__ files - ([7be74ff](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/7be74ffaf2f0194a160fdccbb2c39652b47cf71d))

- changed the case of files names to lower case - ([f8343d8](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/f8343d86479cac0c98b2afdfc066a0c6fdd9a070))

- route url's for deleting, enabling & disabling subscriptions - ([67bd3d9](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/67bd3d9f54bfc197eb137f023806f5182bff9ec4))

- changed the default value of is_active to true in db - ([1de4f4f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/1de4f4f7a186457975aecee15e6a61d6e79cd475))

- referring asName as optional key in onionoo & removed limit of 4 from onionoo api - ([e607e2d](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/e607e2d711ea6d5bea4bd2a528f5284d89f215da))

- made the url to internal api dynamic with env variable - ([789ef83](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/789ef83f5c5d08bbaadb27535f9bea504165ba15))

- server crashing when requesting the subscription-list page - ([316c911](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/316c911d867ac23ec549ef8feac8026324dfc979))

- environment variables failing to load when imported before set - ([57d8051](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/57d80517268dd7784600285e13976f8b78f06cb4))

- cleared extra env variables, added validation for fingerprint, fixed database tables not creating on app init, minor ui glitches - ([b4ec7b9](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b4ec7b9521405d52eb55bae99de5d9036db55ba2))

- typo in node-down email content causing job failure - ([d4e95ad](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/d4e95addf2e6bcb8e3feeb49ec360c1ed8d399fa))


### Documentation

- added readme containing the basic details around setting up the project - ([8ac9ce9](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/8ac9ce9abf0fdd68e52516d698985f987fd438a0))

- updated the readme with a summary of the project - ([9c55cc0](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/9c55cc07166b61e5ce6a5748db414d4c34e505be))

- added description for all environment variables required - ([4ef0211](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/4ef021152db2da4994e47c06e7fa401852f5ca00))


### Features

- added scripts with register, verification & login flow - ([00dab84](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/00dab84a7e2aa1a47a26e80d2fff9176674d3efc))

- completed the user flow improvements involving refactoring & addition of error handling & authorization decorator - ([ace77bc](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/ace77bc41a90a589b4989c249ea85b64c7e80d8d))

- created the dashboard ui - ([d6d1ff8](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/d6d1ff8f86ea94e4ee98c4d03a25ec5f04e7b64c))

- added error routes - ([3ba2ad2](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/3ba2ad26d286fde90ce84d26a498d4a14e7d1da7))

- redone the structure of the app for better & easy maintainability - ([94e789a](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/94e789a1eb02f930df2282446a4d29cc9e755f0c))

- added internal api resources for creating subscriptions - ([5f48293](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/5f48293e0da5a238c0f2e852efb1d86e751c4d4e))

- added backend resources for create, list, modify, delete, enable & disable subscriptions - ([b75c887](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/b75c88718e8399bb06fe9a3c8e153962028963bb))

- created frontend routes for modifying & creating subscriptions - ([9bb446f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/9bb446f37f159a653f707e82ccee78a8e6aba487))

- added snackbar messages for creating, modifying, deleting subs in dashboard - ([a693d58](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/a693d584d0a93b9be96fb6690c0d3b6b118a041e))

- added information-card for create & modify route templates - ([fb89605](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/fb89605d74aa96259cdcf238fff7a6a7f43695cd))

- added scripts with scheduler to run every 1 hour - ([4ecbf4f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/4ecbf4fb30853d1565a50f9f6dd1169b0401f009))

- added content for email templates - ([0243057](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/0243057e7f2dc1daf3d315c0af29504ab03debc0))

- integrated emails with data - ([a99ed21](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/a99ed211f3abdf635c8581cc1b339a5142420c5f))

- added gunicorn wsgi & elevated content from backend to root - ([978be24](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/978be24f06e07549d96b74e800f6450fd47d0010))

- removed first-name & last-name fields from registration form - ([897d7f3](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/897d7f3622cbcf7be6f7de033fcb315637c68fee))

- added license & copyright statement - ([16d1c5f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/16d1c5f250cc2f7ddac7188fa5485f4c429bd658))

- integrated the logging module - ([32c40e3](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/32c40e3961acfbecb3a95a8419775107303ff57c))

- added shell script for deployments - ([974bf30](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/974bf300b7c0e6c5aed50918384cc23231989c1d))

- improved the entry points in the app & segregated job execution from backend - ([2d28f76](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/2d28f765bf76f629474d65b25229f5c9430ea55f))

- modified the app structure to allow error handling - ([bb0a393](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/bb0a393127a3479ba2f39b249f6733bb9eaba964))

- integrated flask-login package to handle auth & auth - ([24e64d6](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/24e64d6f47493d27925efcdb7356de91a4f5aa3d))

- added env variable based config for ssl with smtp - ([a49b718](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/a49b718ea2d7d3a93de6aa97db1cd80afab5ee4f))

- improved error handling - ([1d1a0b7](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/1d1a0b7d9563b63477d66c499cec395d6cbc33fa))

- added error handling in backend - ([bea940b](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/bea940be11ffaaa994f3f16bd03e529ece80d418))

- created separate entry point for data-model - ([ad45046](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/ad450465ce589ec01d53e97202d286cc37a0842e))

- added favicon - ([d05270f](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/d05270f25f98d5ff890d2635928cd35cd63308f2))


### Miscellaneous Chores

- added black, flake8 & bandit in the pipeline - ([27ebf0d](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/27ebf0d2527109b96e061b84fb7004ed0972ad4a))

- added check for testing the dev server and limited the pipeline to be ran on main branch & merge requests to main - ([0855bb3](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/0855bb38460920b9cd59517e7cad31be8f399f92))

- fixed mypy errors & added its precommit, gitlab-ci check  - ([777f32e](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/777f32e46914a653e8d864df68f5a25e6d8bde0c))

- added basic vscode settings for formatting, etc. - ([496e278](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/496e278e34bbf44837411e1c6ac6edd590e7390e))

- changed the port for gunicorn to 5000 & added daemon mode - ([ab14135](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/ab141357105b9a202b437eb377d8f80e9d26566a))

- added support for configuring job interval from environment variables - ([c326e1e](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/c326e1ee84d0fe391a9d1cb4258de07006522be9))

- replaced pipenv with poetry - ([2fc99e0](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/2fc99e0fca324d21f8b835ccba5155bd457c238e))


### Refactoring

- fixed the linting issues with isort addition - ([87dd991](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/87dd991b3107916acd976136b3493427e20c290b))

- modified the routing to be based on a dictionary - ([d9b3555](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/d9b35555c50326d64dd9434c949f5b9ebae4fd36))

- cleaned the app to use flask flash instead of ApiResult module - ([022ed01](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/022ed01e6c2918ff4c99eb3304c4d6a0421a6381))


### Cicd

- improved rules for job-execution - ([45e76ee](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/45e76eef3acc6d17a555627bd688f0a3b264a198))

- added release stage for tag-release & artifact creation - ([e8b9e83](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/e8b9e830fc519e60531c3bb5439252302c044428))

- disabled build job on commit to main - ([0ffea10](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/0ffea10ffec5528e9f11d457cc79c121d84b07b5))

- increment wheel version in pipeline build job - ([63d00aa](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/commit/63d00aae6d67611c2c1fad236650fb193af5a0cb))


